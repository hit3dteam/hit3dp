!================================================================================
! M_IO - module that contains the I/O related subroutines.
!
! Time-stamp: <2010-01-15 19:14:07 (chumakov)>
!================================================================================

module m_io

  use m_openmpi
  implicit none

  character*6  :: file_ext
  character*80 :: fname
  ! output file handle
  integer :: in=10, out=11

  ! level of verbosity of the output
  ! if false, then the process doesn't write any messages in the individual log file
  ! if true, then it opens the file d<myid>.txt and writes there.
  ! note that the master process always has verbose output.
  logical      :: verbose = .false.

!================================================================================
contains
!================================================================================
!================================================================================
  subroutine m_io_init

    implicit none

    if (myid.eq.0) verbose = .true.

    if (verbose) then
       write(fname,"('d',i4.4,'.txt')") myid_world
       open(out,file=fname,position="append")
       write(out,"(80('='))")
       write(out,"('                   HIT3DP, parallel spectral pencil code')")
       write(out,"(80('='))")
       write(out,"(' Process ',i5,' of ',i5,'(',i5.5,') is alive.')") &
            myid_world, numprocs_world, numprocs_world-1
       write(out,"(' My task is ""',a5,'"", my id is',i5)") task, myid
       call flush(out)
    end if

    return
  end subroutine m_io_init

!================================================================================
!================================================================================
  subroutine m_io_exit
    implicit none
    if (verbose) write(out,"('Done.')")
    if (verbose) close(out)
    return
  end subroutine m_io_exit

!================================================================================
!================================================================================
end module m_io
