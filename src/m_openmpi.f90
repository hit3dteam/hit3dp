!================================================================================
! Module contains interface to OpenMPI
!
! Time-stamp: <2009-10-08 11:00:48 (chumakov)>
!================================================================================


module m_openmpi
!================================================================================
  implicit none
  include 'mpif.h'

  ! Uncomment this for the systems that do not have OpenMPI
  ! In OpenMPI, the parameter MPI_INTEGER_KIND is defined in 'mpif.h'
  ! With other MPI implementations, this parameter has to be defined manually.
!  integer MPI_INTEGER_KIND
!  parameter (MPI_INTEGER_KIND = 4)

  ! --- MPI variables
  logical :: iammaster
  integer (kind=MPI_INTEGER_KIND) :: myid_world, numprocs_world
  integer (kind=MPI_INTEGER_KIND) :: numprocs_hydro, numprocs_stats, numprocs_parts
  integer (kind=MPI_INTEGER_KIND) :: myid, numprocs, master, mpi_err, mpi_info
  integer (kind=MPI_INTEGER_KIND) :: id_to, id_from, tag, count
  integer (kind=MPI_INTEGER_KIND) :: id_root_hydro, id_root_stats, id_root_parts
  integer (kind=mpi_INTEGER_KIND) :: mpi_status(MPI_STATUS_SIZE)

  ! communicator for separate tasks (e.g., hyrdo, stats, particles)
  integer (kind=MPI_INTEGER_KIND) :: MPI_COMM_TASK

  integer (kind=MPI_INTEGER_KIND) :: sendtag, recvtag
  integer (kind=MPI_INTEGER_KIND) :: request, request1, request2, request3, mpi_request

  ! for splitting the WORLD communicator into several communicators
  integer(kind=MPI_INTEGER_KIND) :: color, key

  ! the row and column of the processor (z- and y- coordinate of teh "pencil")
  integer (kind=MPI_INTEGER_KIND) :: my_row, my_col

  character*5 :: task, split="never"
  character*10 :: run_name_local

  logical :: task_split=.false.

!================================================================================
contains
!================================================================================

  subroutine m_openmpi_init

    implicit none

    integer (kind=mpi_INTEGER_KIND) :: n
    integer*4 :: np_local
    integer :: i

    ! first getting the run name form the command line 
    ! (it's local, not  global run_name)
    ! also getting the parameter "split" which governs the process splitting:
    ! split="split" means that hydro, statistics and particles are assigned three 
    ! separate process groups (they differ by the char*5 parameter "task").
    ! split="never" (default if the parameter is missing) means that all
    ! processes do all tasks. (does not work for the particles at this point)
    call openmpi_get_command_line


    ! initializing MPI environment
    call MPI_INIT(mpi_err)
    call MPI_Comm_size(MPI_COMM_WORLD,numprocs_world,mpi_err)
    call MPI_Comm_rank(MPI_COMM_WORLD,myid_world,mpi_err)

!--------------------------------------------------------------------------------
!  Looking at the command line parameter called "split".  If it equals "split"
!  then we define task_split=.true.  If not, task_split remains .false. (default)
!--------------------------------------------------------------------------------
    if (split == "split") task_split = .true.
    if (split == "split") stop "CANNOT SPLIT TASKS AT THIS POINT"

!--------------------------------------------------------------------------------
!  First check if we need to do any task splitting.  If we don't (split="never")
!  then we define task="hydro" and do a ficticious split with uniform color of
!  all processors.
!--------------------------------------------------------------------------------
    if (.not. task_split) then
       task = 'hydro'
       color = 0
       myid = myid_world
       goto 1000
    end if

!--------------------------------------------------------------------------------   
!  The actual task splitting happens here
!--------------------------------------------------------------------------------   
1000 continue
    call MPI_COMM_SPLIT(MPI_COMM_WORLD,color,myid,MPI_COMM_TASK,mpi_err)
    call MPI_COMM_SIZE(MPI_COMM_TASK,numprocs,mpi_err)
    call MPI_COMM_RANK(MPI_COMM_TASK,myid,mpi_err)

    ! each task will have its master process 
    master = 0
    iammaster = .false.
    if (myid.eq.master) iammaster=.true.


!--------------------------------------------------------------------------------
!  Figuring out the size of pencils
!--------------------------------------------------------------------------------
!  The domain is split into pencils with the size (nx,ny,nz).  NX is the 
!  largest dimension, along the first index (i), NY and NZ are the sizes 
!  along the second and third indicies (j,k).  They are defined to be
!  ny = nx / pencils_y, nz = nx / pencils_z (this definition happens in
!  m_parameters.f90).  Here we need to define pencils_y and pencils_z

!  (left for later)

    return
  end subroutine m_openmpi_init

!================================================================================

  subroutine m_openmpi_exit

    call MPI_COMM_FREE(MPI_COMM_TASK,mpi_err)
    call MPI_FINALIZE(mpi_err)

    return
  end subroutine m_openmpi_exit


!================================================================================

  subroutine openmpi_get_command_line
    implicit none

    character*80 :: tmp_str
    integer      :: iargc

    ! reading the run_name from the command line
    if(iargc().eq.0) then
       call getarg(0,tmp_str)
       print*, 'Format: ',trim(tmp_str),' (run name) ["split"/"never"]'
       stop
    end if
    call getarg(1,run_name_local)
    if(len_trim(run_name_local).ne.10) then
       print *, 'Run name: "',run_name_local,'"'
       print *, '          "1234567890"'
       print *, 'Length of run name is less than 10, sorry.'
       stop
    end if

    ! getting the split parameter, if it's there
    if(iargc().eq.2) call getarg(2,split)


  end subroutine openmpi_get_command_line

!================================================================================

end module m_openmpi
