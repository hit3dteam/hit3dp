!==============================================================================!
!
!  pseudospectral DNS code, Fast Fourier Transform that uses FFTW3 library
!  Copyright (C) 2006-2009 Sergei Chumakov, Misha Stepanov, Natalia Vladimirova
!
!  This program is free software; you can redistribute it and/or modify
!  it under the terms of the GNU General Public License as published by
!  the Free Software Foundation; either version 2 of the License, or
!  (at your option) any later version.
!
!  This program is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU General Public License for more details.
!
!  You should have received a copy of the GNU General Public License
!  along with this program.  If not, see <http://www.gnu.org/licenses/>.
!
!==============================================================================!

!  The n x n x n scalar field wrk(:, :, :, k) is divided into "pencils",
!  n x small_n_fast x small_n_slow each. Size n should be divisible by
!  small_n_fast and by small_n_slow exactly. The y-direction is divided
!  into pencils_fast = n / small_n_fast, and z-direction into
!  pencils_slow = n / small_n_slow subintervals.
!
!                                   A y
!                                   |
!           +---+---+---+---+---+---+
!          /   /   /   /   /   /   /|
!         /   /   /   /   /   /   / |
!        /   /   /   /   /   / wrk(:, :, :, k)
!       /   /   /   /   /   /   /   +
!      +---+---+---+---+---+---+   /|
!      |   |   |   |   |   |   |  / |
!      | h | e | b | 8 | 5 | 2 | /  |
!      |   |   |   |   |   |   |/   +
!      +---+---+---+---+---+---+   /|
!      |   |   |   |   |   |   |  / |
!      | g | d | a | 7 | 4 | 1 | /  |
!  <---|   |   |   |   |   |   |/   +
!  z   +---+---+---+---+---+---+   /
!      |   |   |   |   |   |   |  /
!      | f | c | 9 | 6 | 3 | 0 | /
!      |   |   |   |   |   |   |/
!      +---+---+---+---+---+---+
!                             /
!                            V x
!
! The numeration of pencils (myid) goes as shown on the picture.
! On this picture pencils_fast = 3, pencils_slow = 6.

MODULE m_fftw_pencil

!==============================================================================!
!  VARIABLES
!==============================================================================!
  use m_openmpi
  use m_parameters
  use m_io
  use m_fields
  use m_work

  use m_timing

  implicit none

  ! FFTW parameters that do not change
  integer(kind=8), parameter :: FFTW_ESTIMATE = 0
  integer(kind=8), parameter :: FFTW_MEASURE  = 1
  integer(kind=8), parameter :: FFTW_FORWARD  = -1
  integer(kind=8), parameter :: FFTW_BACKWARD =  1

  ! dedicated arrays for parallel FFT
  real(kind=8), allocatable :: stick(:)
  real(kind=8), allocatable :: buf1s(:, :, :), buf1r(:, :, :)
  real(kind=8), allocatable :: buf2s(:, :, :), buf2r(:, :, :)

  ! variables for FFTW plans
  integer(kind=8) :: plan_r2c, plan_c2r, plan_f_c2c, plan_b_c2c

  ! ----------------------------------------------------------------------
  ! new generation of FFT transforms
  integer(kind=8) :: plan_r2c_whole, plan_c2r_whole, planbbb
  integer(kind=8), allocatable :: plan_f_c2c_whole(:), plan_b_c2c_whole(:)
  real(kind=8), allocatable :: tmp83(:,:,:)

  ! ----------------------------------------------------------------------

  ! arrays for the wavenumbers
  real(kind=8), allocatable :: akx(:), aky(:), akz(:)

  ! array for dealiasing
  integer (kind=1), allocatable :: ialias(:,:,:)

  ! auxiliary parameters
  real(kind=8) :: norm

  ! conversion from the notations of the outer code to the notations of this
  ! module
  integer :: pencils_fast, pencils_slow, small_n_fast, small_n_slow

  ! these variables could be defined globally
  integer(kind=MPI_INTEGER_KIND) :: myid_div_pf, myid_mod_pf
  integer(kind=MPI_INTEGER_KIND) :: my_slab_y, my_slab_z


  integer(kind=MPI_INTEGER_KIND) :: iproc
  real (kind=8) :: rtmp


  ! These are for benchmarking
  integer*8 :: bm_forward, bm_backward, i81, i82, bm(12)

!==============================================================================!
!==============================================================================!
CONTAINS
!==============================================================================!
!==============================================================================!
!  SUBROUTINES
!==============================================================================!
!==============================================================================!
!  Subroutine that performs the FFT of a 3-D variable.  The variable is
!  contained within the array "wrk(:, :, :, n)".  Note that the
!  result of FFT has different coordinate arrangement: in physical
!  space it is (x, y, z), and in Fourier space it is (ky, kz, kx).
!  Details can be extracted from very graphic comments in the body of
!  the subroutine.
!==============================================================================!
!================================================================================
subroutine fftw3d(flag, n)
  implicit none
  integer :: flag, n
  call my_fftw3d(flag, n)
  !call misha_fftw3d(flag,n)
  return
end subroutine fftw3d
!================================================================================
subroutine my_fftw3d(flag, n)
  implicit none
  integer :: flag, n
  !if (iammaster) print *,'my fft', flag
  if (flag == 1) then

    if(benchmarking) then
       call system_clock(i81,dcpu)
       bm(11) = bm(11) - i81
    end if

    tmp83(1:nx,:,:) = wrk(:,:,:,n)
    call dfftw_execute(plan_r2c_whole)
    wrk(:,:,:,n) = tmp83(1:nx,:,:)

    if (benchmarking) then
       call system_clock(i82,dcpu)
       bm(1) = bm(1) + i82 - i81
       i81 = i82
    end if

    call m_fftw_transpose_13(n)

    if (benchmarking) then
       call system_clock(i82,dcpu)
       bm(2) = bm(2) + i82 - i81
       i81 = i82
    end if

    call dfftw_execute(plan_f_c2c_whole(n))

    if (benchmarking) then
       call system_clock(i82,dcpu)
       bm(3) = bm(3) + i82 - i81
       i81 = i82
    end if

    call m_fftw_transpose_12(n)

    if (benchmarking) then
       call system_clock(i82,dcpu)
       bm(4) = bm(4) + i82 - i81
       i81 = i82
    end if

    call dfftw_execute(plan_f_c2c_whole(n))

    if (benchmarking) then
       call system_clock(i82,dcpu)
       bm(5) = bm(5) + i82 - i81
       bm(11) = bm(11) + i82
    end if

  else

    if(benchmarking) then
       call system_clock(i81,dcpu)
       bm(12) = bm(12) - i81
    end if

    call dfftw_execute(plan_b_c2c_whole(n))

    if (benchmarking) then
       call system_clock(i82,dcpu)
       bm(6) = bm(6) + i82 - i81
       i81 = i82
    end if

    call m_fftw_transpose_12(n)

    if (benchmarking) then
       call system_clock(i82,dcpu)
       bm(7) = bm(7) + i82 - i81
       i81 = i82
    end if

    call dfftw_execute(plan_b_c2c_whole(n))

    if (benchmarking) then
       call system_clock(i82,dcpu)
       bm(8) = bm(8) + i82 - i81
       i81 = i82
    end if

    call m_fftw_transpose_13(n)

    if (benchmarking) then
       call system_clock(i82,dcpu)
       bm(9) = bm(9) + i82 - i81
       i81 = i82
    end if

    tmp83(1:nx,:,:) = wrk(:,:,:,n)
    tmp83(nx+1:nx+2,:,:) = zip
    call dfftw_execute(plan_c2r_whole)
    wrk(:,:,:,n) = tmp83(1:nx,:,:) * norm

    if (benchmarking) then
       call system_clock(i82,dcpu)
       bm(10) = bm(10) + i82 - i81
       bm(12) = bm(12) + i82
    end if

  end if

  return
  end subroutine my_fftw3d


!================================================================================
!================================================================================
!================================================================================
  subroutine misha_FFTW3D(flag, n)

    use m_openmpi
    implicit none

    integer :: flag, n
    integer :: i, j, k, ix, iy, iz

    real(kind=8) :: rtmp

    integer(kind=MPI_INTEGER_KIND) :: iproc

    ! if (iammaster) print *,'misha fft', flag
    if (flag == 1) then

!------------------------------------------------------------------------------!
!  Direct FFT, first step: 1-D real-to-complex transform in x direction
!------------------------------------------------------------------------------!
!  
!   R2C         A y                                      A y
!               |                                        |
!           +---+                                    +---+
!          /   /|                                   /   /|
!         /   / |       stick    stick             /   / |
!        / wrk  |          +        +             / wrk  |
!  <----/   /   +         /  R2C   /        <----/   /   +
!  z   +---+   /  -----> / -----> / ----->  z   +---+   /
!      |   |  /         /        /              |   |  /
!      |   | /         /        /               |   | /
!      |   |/         +        +                |   |/
!      +---+                                    +---+
!         /                                        /
!        V x                                      V k_x
!
!------------------------------------------------------------------------------!

       do iz = 1, small_n_slow
          do iy = 1, small_n_fast
             do ix = 1, nx
                stick(ix) = wrk(ix, iy, iz, n)
             end do
             call DFFTW_EXECUTE(plan_r2c)
             ! this KILLS Nyquist frequency in x-direction
             do ix = 1, nx
                wrk(ix, iy, iz, n) = stick(ix)
             end do
          end do
       end do

       call system_clock(i82,dcpu)
       bm(1) = bm(1) + i82 - i81
       i81 = i82

       call M_FFTW_TRANSPOSE_13(n)

       call system_clock(i82,dcpu)
       bm(2) = bm(2) + i82 - i81
       i81 = i82

       call M_FFTW_1D_C2C(flag, n)

       call system_clock(i82,dcpu)
       bm(3) = bm(3) + i82 - i81
       i81 = i82

       call M_FFTW_TRANSPOSE_12(n)

       call system_clock(i82,dcpu)
       bm(4) = bm(4) + i82 - i81
       i81 = i82


       call M_FFTW_1D_C2C(flag, n)

       call system_clock(i82,dcpu)
       bm(5) = bm(5) + i82 - i81
       i81 = i82

    elseif (flag == -1) then

       call M_FFTW_1D_C2C(flag, n)

       call M_FFTW_TRANSPOSE_12(n)

       call M_FFTW_1D_C2C(flag, n)

       call M_FFTW_TRANSPOSE_13(n)

!------------------------------------------------------------------------------!
!  Inverse FFT, last step: 1-D complex-to-real transform in x direction
!------------------------------------------------------------------------------!
       do iz = 1, small_n_slow
          do iy = 1, small_n_fast
             do ix = 1, nx
                stick(ix) = wrk(ix, iy, iz, n)
             end do
             ! this MAKES Nyquist frequency being 0 in x-direction
             stick(nx + 1) = 0.
             stick(nx + 2) = 0.
             call DFFTW_EXECUTE(plan_c2r)
             do ix = 1, nx
                wrk(ix, iy, iz, n) = norm * stick(ix)
             end do
          end do
       end do

    end if ! flag (direct of inverse)

    return
  end subroutine misha_FFTW3D

!==============================================================================!
!  Subroutine that allocates/deallocates the FFTW arrays
!==============================================================================!
  subroutine M_FFTW_ALLOCATE(flag)

    implicit none
    integer :: flag

    pencils_fast = nx / ny
    pencils_slow = nx / nz
    myid_div_pf = myid / pencils_fast
    myid_mod_pf = mod(myid, pencils_fast)
    small_n_fast = ny
    small_n_slow = nz

    my_slab_y = mod(myid, pencils_fast)
    my_slab_z = myid / pencils_fast

    if (flag == 1) then

       allocate( &
            buf1s(small_n_slow, small_n_fast, small_n_slow), &
            buf1r(small_n_slow, small_n_fast, small_n_slow), &
            buf2s(small_n_fast, small_n_fast, small_n_slow), &
            buf2r(small_n_fast, small_n_fast, small_n_slow), &
            stick(2 * nx), &
            akx(nz), aky(nx), akz(ny), &
            ialias(nx,ny,nz), &
            stat = ierr) 

       if (ierr /= 0) then
          write (out, *) '*** M_FFTW_ALLOCATE: cannot allocate'
          call my_exit(-1)
       end if

       if (verbose) write(out,*) "m_fftw_allocated."
       if (verbose) call flush(out)

       ! assigning temporary values to allocated arrays
       stick = zip
       buf1s = zip
       buf1r = zip
       buf2s = zip
       buf2r = zip
       ialias = 0

    elseif (flag == -1) then
       if (allocated(stick)) deallocate(stick, buf1s, buf1r, buf2s, buf2r)
       if (verbose) write(out,*) "m_fftw_deallocated."
       if (verbose) call flush(out)
    else
       write (out, *) '*** M_FFTW_ALLOCATE: wrong value of flag:', flag
       call my_exit(-1)
    end if

    bm = 0

    return
  end subroutine M_FFTW_ALLOCATE

!==============================================================================!
!  Program that initializes the auxilary arrays for FFT
!==============================================================================!

  subroutine M_FFTW_INIT

    implicit none
    integer :: i, j, k, n
    real*8  :: rnx3

    integer(kind=4) :: howmany, inembed, istride, idist, onembed, ostride, odist
    integer(kind=4) :: rank, dims(1), dims_is(1), dims_os(1)
    integer(kind=4) :: howmany_rank, howmany_dims(3), howmany_dims_is(3), howmany_dims_os(3)

    if (verbose) write(out, *) 'Initializing FFT arrays.'
    if (verbose) call flush(out)

!------------------------------------------------------------------------------!
!  initializing FFTW plan for the 1D r2c and c2r
!------------------------------------------------------------------------------!
    call DFFTW_PLAN_DFT_R2C_1D(plan_r2c, nx, stick, stick, FFTW_ESTIMATE)
    call DFFTW_PLAN_DFT_C2R_1D(plan_c2r, nx, stick, stick, FFTW_ESTIMATE)
!!$    call DFFTW_PLAN_DFT_R2C_1D(plan_r2c, nx, stick, stick, FFTW_MEASURE)
!!$    call DFFTW_PLAN_DFT_C2R_1D(plan_c2r, nx, stick, stick, FFTW_MEASURE)

!------------------------------------------------------------------------------!
!  initializing FFTW plan for the forward and backward 1D c2c
!------------------------------------------------------------------------------!
    call DFFTW_PLAN_DFT_1D(plan_f_c2c, nx, stick, stick, &
         FFTW_FORWARD, FFTW_ESTIMATE)
    call DFFTW_PLAN_DFT_1D(plan_b_c2c, nx, stick, stick, &
         FFTW_BACKWARD, FFTW_ESTIMATE)
!!$
!!$    call DFFTW_PLAN_DFT_1D(plan_f_c2c, nx, stick, stick, &
!!$         FFTW_FORWARD, FFTW_MEASURE)
!!$    call DFFTW_PLAN_DFT_1D(plan_b_c2c, nx, stick, stick, &
!!$         FFTW_BACKWARD, FFTW_MEASURE)

!--------------------------------------------------------------------------------
!  Multiple transforms at once
    if (.not.allocated(tmp83)) allocate(tmp83(nx+2,ny,nz))
    allocate(plan_f_c2c_whole(LBOUND(wrk,4):UBOUND(wrk,4)), &
         plan_b_c2c_whole(LBOUND(wrk,4):UBOUND(wrk,4)))
    plan_f_c2c_whole = 0
    plan_b_c2c_whole = 0
!!$    if (iammaster) print *, plan_f_c2c_whole
!!$    if (iammaster) print *, plan_b_c2c_whole
!!$    if (iammaster) print *,'--'

    ! plan for the fast R2C one-dimensional transform of three-dimensional array
    rank = 1
    dims(1) = nx
    howmany = ny*nz
    inembed = (nx+2)*ny*nz
    istride = 1
    idist   = nx+2
    onembed = (nx+2)*ny*nz/2
    ostride = 1
    odist = (nx+2)/2
    CALL DFFTW_PLAN_MANY_DFT_R2C(plan_r2c_whole, rank, dims, howmany, &
         tmp83(1,1,1), inembed, istride, idist, &
         tmp83(1,1,1), onembed, ostride, odist, FFTW_ESTIMATE, ierr)

    ! plan for the fast C2R one-dimensional transform of three-dimensional array
    rank = 1
    dims(1) = nx
    howmany = ny*nz
    inembed = (nx/2+1)*ny*nz
    istride = 1
    idist = (nx+2)/2
    onembed = (nx+2)*ny*nz
    ostride = 1
    odist = nx+2
    CALL DFFTW_PLAN_MANY_DFT_C2R(plan_c2r_whole, rank, dims, howmany, &
         tmp83(1,1,1), inembed, istride, idist, &
         tmp83(1,1,1), onembed, ostride, odist, FFTW_ESTIMATE, ierr)

    ! plan for fast C2C one-dimensional transform of a 3d array, in which
    ! the complex numbers are aligned with the third (slowest) index.  That is,
    ! a complex number is a(i,j,k) + ii * a(i,j,k+1), where ii = sqrt(-1).
    ! For this, we need to use the guru interface to FFTW with the split
    ! input and output arrays (for more information, see FFTW manual)

    rank = 1
    dims(1) = nx    ! dimension of a single transform
    dims_is(1) = 1  ! stride in the input
    dims_os(1) = 1  ! stride in the output

    ! the dimensionality of the large array in which the transform is embedded
    howmany_rank = 3  

    ! dimensions of the big array.  Units are real*8, because it's a split call.
    ! note the switch of indicies because Fortran has column-major format
    howmany_dims(1) = nz/2  ! # of transforms stacked in z-direction
    howmany_dims(2) = ny    ! # of transforms stacked in y-direction
    howmany_dims(3) = 1     ! # of transforms stacked in x-direction

    ! strides for input and output.  Units are double, NOT complex
    howmany_dims_is(1) = nx*ny*2  ! one change in z-coordinate corresponds to this shift in input
    howmany_dims_is(2) = nx       ! one change in y-coordinate corresponds to this shift in input
    howmany_dims_is(3) = 1        ! one change in x-coordinate corresponds to this shift in input

    howmany_dims_os(1) = nx*ny*2  ! one change in z-coordinate corresponds to this shift in output
    howmany_dims_os(2) = nx       ! one change in y-coordinate corresponds to this shift in output
    howmany_dims_os(3) = 1        ! one change in x-coordinate corresponds to this shift in output

    do n = LBOUND(wrk,4), UBOUND(wrk,4)
       ! forward C2C transform of wrk(:,:,:,k) with complex numbers aligned along the 3rd index
       call DFFTW_PLAN_GURU_SPLIT_DFT(plan_f_c2c_whole(n), &
            rank, dims, dims_is, dims_os, howmany_rank, howmany_dims, howmany_dims_is, howmany_dims_os,&
            wrk(1,1,1,n), wrk(1,1,2,n), wrk(1,1,1,n), wrk(1,1,2,n), FFTW_ESTIMATE, ierr)
       ! backward C2C transform of wrk(:,:,:,k) with complex numbers aligned along the 3rd index
       ! we use the identity that backward FFT equals the forward FFT with real and imaginary
       ! parts swapped, as described in FFTW manual, section 4.5.3
       call DFFTW_PLAN_GURU_SPLIT_DFT(plan_b_c2c_whole(n), &
            rank, dims, dims_is, dims_os, howmany_rank, howmany_dims, howmany_dims_is, howmany_dims_os,&
            wrk(1,1,2,n), wrk(1,1,1,n), wrk(1,1,2,n), wrk(1,1,1,n), FFTW_ESTIMATE, ierr)
    end do

!------------------------------------------------------------------------------!
!  initializing some useful constants
!------------------------------------------------------------------------------!
    norm = one / real(nx**3, 8)

    if (verbose) write(out,*) "m_fftw_pencil.f90 arrays are intiialized."
    if (verbose) call flush(out)

    ! intializing wavenumbers
    ! akx: along the z-axis in Fourier space
    do k = 1, nz-1, 2
       akx(k  ) = real( (my_slab_z*nz + k - 1) / 2, 8)
       akx(k+1) = akx(k)
    end do
    ! aky: along the x-axis in Fourier space
    do i = 1, nx
       aky(i) = real(i - 1, 8)
       if (aky(i) > (0.5D0 * real(nx, 8))) aky(i) = aky(i) - real(nx, 8)
    end do
    ! akz: along the y-axis in Fourier space
    do j = 1, ny
       akz(j) = real( my_slab_y*ny + j - 1, 8)
       if (akz(j) > (0.5D0 * real(nx, 8))) akz(j) = akz(j) - real(nz_all, 8)
    end do

!--------------------------------------------------------------------------------
!  Initializing the array ialias that contains the number of wavenumbers at 
!  (i,j,k) that have their magnitude higher than nx/3.  
!  This is needed in dealiasing procedures.
!--------------------------------------------------------------------------------
    rnx3 = real(nx/3, 8)
    do k = 1, nz
       if (abs(akx(k)) .gt. rnx3) ialias(:,:,k) = 1
       do j = 1, ny
          if (abs(akz(j)) .gt. rnx3) ialias(:,j,k) = ialias(:,j,k) + 1
          do i = 1, nx
             if (abs(aky(i)) .gt. rnx3) ialias(i,j,k) = ialias(i,j,k) + 1
          end do
       end do
    end do

!--------------------------------------------------------------------------------
    return
  end subroutine M_FFTW_INIT

!==============================================================================!

  subroutine M_FFTW_1D_C2C(flag, k)

    implicit none
    integer :: flag, k, ix, iy, iz

!------------------------------------------------------------------------------!
!  1D complex-to-complex FFT transform
!------------------------------------------------------------------------------!
!  
!   C2C         A B                                      A B
!               |                                        |
!           +---+                                    +---+
!          /   /|                                   /   /|
!         /   / |       stick    stick             /   / |
!        / wrk  |          +        +             / wrk  |
!  <----/   /   +         /  C2C   /        <----/   /   +
!  C   +---+   /  -----> / -----> / ----->  C   +---+   /
!      |   |  /         /        /              |   |  /
!      |   | /         /        /               |   | /
!      |   |/         +        +                |   |/
!      +---+                                    +---+
!         /                                        /
!        V A                                      V k_A
!
!------------------------------------------------------------------------------!
    do ix = 1, small_n_slow / 2
       do iy = 1, small_n_fast
          do iz = 1, nx
             stick(2*iz-1) = wrk(iz, iy, 2 * ix - 1, k)
             stick(2*iz  ) = wrk(iz, iy, 2 * ix    , k)
          end do

          if (flag == 1) then
             call DFFTW_EXECUTE(plan_f_c2c)
          else
             call DFFTW_EXECUTE(plan_b_c2c)
          endif

          do iz = 1, nx
             wrk(iz, iy, 2 * ix - 1, k) = stick(2 * iz - 1)
             wrk(iz, iy, 2 * ix    , k) = stick(2 * iz)
          end do
       end do
    end do

    return
  end subroutine M_FFTW_1D_C2C

!==============================================================================!

  subroutine M_FFTW_TRANSPOSE_13(k)

    implicit none
    integer :: k, ix, iy, iz, iproc, idto_small

!------------------------------------------------------------------------------!
!  transposing the variable via MPI messaging
!------------------------------------------------------------------------------!
!
!   MPI                     .----.  A B
!                          /      \ |      buf1          buf1
!           +---+---+---+-/-+---+--\+     +---+         +---+
!          /   /   /   / / /   /   /\    +---+|  MPI   +---+|
!         /   /   /   /+++/   /   / |`-> |   || -----> |   ||
!        /   /   /   /.../   / wrk  |    |   ||        |   ||
!  <----/   /   /   / ../.  /   /   +    |   |+        |   |+
!  C   +---+---+---+---+---+---+   /     +---+         +---+               A B
!      |   |   |   |  .|.. |   |  /                       |                |
!      | g | d | a |  .|.  | 1 | /                 +---+--|+---+---+---+---+
!      |   |   |   |   |   |   |/                 /   /   |   /   /   /   /|
!      +---+---+---+---+---+---+                 /   /   /V  /   /   /   / |
!                             /                 /   /   /+++/   /   / wrk  |
!  5 4 3 2 1 6               V A          <----/   /   /.../   /   /   /   +
!  4 3 2 1 6 5                            A   +---+---+---+---+---+---+   /
!  3 2 1 6 5 4                                |   |   | ..|.  |   |   |  /
!  2 1 6 5 4 3                                | g | d | ..|.  | 4 | 1 | /
!  1 6 5 4 3 2                                |   |   | ..|   |   |   |/
!  6 5 4 3 2 1 --- on iproc round the         +---+---+---+---+---+---+
!                  following pencils                                 /
!  g d a 7 4 1     parts are exchanged                              V C
!
!------------------------------------------------------------------------------!
    count = small_n_slow * small_n_fast * small_n_slow

    do iproc = 1, pencils_slow

       idto_small = iproc - 1 - myid_div_pf
       if (idto_small < 0) idto_small = idto_small + pencils_slow
       id_to = idto_small * pencils_fast + myid_mod_pf

       if (id_to /= myid) then ! MPI exchange of data
          do iz = 1, small_n_slow
             do iy = 1, small_n_fast
                do ix = 1, small_n_slow
                   buf1s(ix, iy, iz) = wrk(idto_small * small_n_slow + ix, iy, iz, k)
                end do
             end do
          end do
!          if (verbose) write(out,*) "### Data exchange with ", myid, " ---> ", id_to
!          if (verbose) write(out,*) "myid_div_pf = ", myid_div_pf, "; idto_small = ", idto_small
!          if (verbose) call flush(out)
!          call MPI_SENDRECV( &
!               buf1s, count, MPI_REAL8, id_to, myid * numprocs + id_to, &
!               buf1r, count, MPI_REAL8, id_to, id_to * numprocs + myid, &
!               MPI_COMM_TASK, mpi_status, mpi_err) 
          call MPI_SENDRECV( &
               buf1s, count, MPI_REAL8, id_to,  myid, &
               buf1r, count, MPI_REAL8, id_to, id_to, &
               MPI_COMM_TASK, mpi_status, mpi_err) 
          do iz = 1, small_n_slow
             do iy = 1, small_n_fast
                do ix = 1, small_n_slow
                   wrk(idto_small * small_n_slow + iz, iy, ix, k) = buf1r(ix, iy, iz)
                end do
             end do
          end do

       else ! no MPI is needed
!          if (verbose) write(out,*) "### Data exchange with ", myid, " ---> ", id_to
!          if (verbose) write(out,*) "myid_div_pf = ", myid_div_pf, "; idto_small = ", idto_small
!          if (verbose) call flush(out)
          do iz = 1, small_n_slow - 1
             do iy = 1, small_n_fast
                do ix = iz + 1, small_n_slow
                   rtmp = wrk(myid_div_pf * small_n_slow + ix, iy, iz, k)
                   wrk(myid_div_pf * small_n_slow + ix, iy, iz, k) = &
                        & wrk(myid_div_pf * small_n_slow + iz, iy, ix, k)
                   wrk(myid_div_pf * small_n_slow + iz, iy, ix, k) = rtmp
                end do
             end do
          end do

       end if ! MPI or not
    end do ! iproc

    return
  end subroutine M_FFTW_TRANSPOSE_13

!==============================================================================!

  subroutine M_FFTW_TRANSPOSE_12(k)

    implicit none
    integer :: k, ix, iy, iz, idto_small, i1, j1, k1

!------------------------------------------------------------------------------!
!  transposing the variable via MPI messaging
!------------------------------------------------------------------------------!
!
!   MPI         A B                                       A A
!               |                                         |
!           +++++                                     +---+
!          /.../|                                    /   /|
!         /.../.\                                   /   / |
!        / wrk..|\                                 / wrk  |
!       /  ./...+ \       buf2           buf2     /   /   +
!      +---+.../|  \     +---+          +---+    +---+   /|
!      |   |  / |   `-> /   /|         /   /|    |   |  / |
!      | 8 | /  |      +---+ |  MPI   +---+ |    | 8 | /  |
!      |   |/   +      |   | | -----> |   | |    |   |/   +
!      +---+   /|      |   | +        |   | +    +---+   /|
!      |   |  / |      |   |/         |   |/     |   |  / |
!      | 7 | /  |      +---+          +---+      | 7.|./  |
!  <---|   |/   +                        \   <---|...|/.  +
!  C   +---+   /   2 1 3                  \  C   +++++.. /
!      |   |  /    1 3 2                   \     |...|../
!      | 6 | /     3 2 1 --- on iproc round `---->.6.|./
!      |   |/                the following       |...|/
!      +---+       8 7 6     pencils parts       +---+
!         /                  are exchanged          /
!        V A                                       V B
!
!------------------------------------------------------------------------------!
    count = small_n_fast * small_n_fast * small_n_slow

!    if (verbose) write(out,*) 'in transpose_12, count = ', count, small_n_fast, small_n_slow
!    if (verbose) call flush(out)

    do iproc = 1, pencils_fast

       idto_small = iproc - 1 - myid_mod_pf
       if (idto_small < 0) idto_small = idto_small + pencils_fast
       id_to = idto_small + myid_div_pf * pencils_fast

!       if (verbose) write(out,*) "iproc = ", iproc, id_to
!       if (verbose) call flush(out)

       if (id_to /= myid) then ! MPI exchange of data
!!$          do ix = 1, small_n_slow
!!$             do iy = 1, small_n_fast
!!$                do iz = 1, small_n_fast
!!$                   buf2s(iz, iy, ix) = wrk(idto_small * small_n_fast + iz, iy, ix, k)
!!$                end do
!!$             end do
!!$          end do
          do k1 = 1,nz
             do j1 = 1, ny
                do i1 = 1, ny
                   buf2s(i1,j1,k1) = wrk(idto_small*ny+i1, j1, k1, k)
                end do
             end do
          end do

!!$          if (verbose) write(out,*) "Data xchange with proc #", id_to
!!$          if (verbose) call flush(out)

          call MPI_SENDRECV( &
               buf2s, count, MPI_REAL8, id_to,  myid, &
               buf2r, count, MPI_REAL8, id_to, id_to, &
               MPI_COMM_TASK, mpi_status, mpi_err)

!          call MPI_SENDRECV( &
!               buf2s, count, MPI_REAL8, id_to, myid * numprocs + id_to, &
!               buf2r, count, MPI_REAL8, id_to, id_to * numprocs + myid, &
!               MPI_COMM_TASK, mpi_status, mpi_err)

!!$          do iz = 1, small_n_slow
!!$             do iy = 1, small_n_fast
!!$                do ix = 1, small_n_slow
!!$                   wrk(idto_small * small_n_fast + iy, iz, ix, k) = buf2r(iz, iy, ix)
!!$                end do
!!$             end do
!!$          end do

          do k1 = 1,nz
             do j1 = 1,ny
                do i1 = 1,ny
                   wrk(idto_small*ny+i1, j1, k1, k) = buf2r(j1, i1, k1)
                end do
             end do
          end do


       else ! no MPI is needed
          do ix = 1, small_n_slow
             do iz = 1, small_n_fast - 1
                do iy = iz + 1, small_n_fast
                   rtmp = wrk(myid_mod_pf * small_n_fast + iz, iy, ix, k)
                   wrk(myid_mod_pf * small_n_fast + iz, iy, ix, k) = &
                        & wrk(myid_mod_pf * small_n_fast + iy, iz, ix, k)
                   wrk(myid_mod_pf * small_n_fast + iy, iz, ix, k) = rtmp
                end do
             end do
          end do

       end if ! MPI or not
    end do ! iproc

    return
  end subroutine M_FFTW_TRANSPOSE_12

!================================================================================
!================================================================================

  subroutine fft_derivative(m, dir, n)

    implicit none
    character :: dir
    integer   :: m, n

    integer :: i, j, k

    select case (dir)

    case('x')
       do k = 1, nz
          wrk(:,:,k,n) = wrk(:,:,k,m) * akx(k)
       end do
    case('y')
       do k = 1, nz
          do j = 1, ny
             wrk(:,j,k,n) = wrk(:,j,k,m) * aky(:)
          end do
       end do
    case('z')
       do k = 1, nz
          do j = 1, ny
             wrk(:,j,k,n) = wrk(:,j,k,m) * akz(j)
          end do
       end do
    case default
       if (verbose) write(out,*) "*** FFT_DERIVATIVE: unknown direction: ",dir
       if (verbose) call flush(out)
       call my_exit(-1)
    end select

    call multiply_by_i(n)

    return
  end subroutine fft_derivative

!================================================================================
!  Multiplication by "i"

  subroutine multiply_by_i(n)

    implicit none
    integer :: i, j, k, n

    do k = 1, nz-1, 2
       do j = 1, ny
          do i = 1, nx
             rtmp = wrk(i, j, k, n)
             wrk(i, j, k  , n) = - wrk(i, j, k+1, n)
             wrk(i, j, k+1, n) = rtmp
          end do
       end do
    end do
    return
  end subroutine multiply_by_i

!================================================================================

end MODULE M_FFTW_PENCIL
