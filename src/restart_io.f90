!================================================================================
!================================================================================
!================================================================================
subroutine restart_write

!  This routine is called to GENERATE RESTART FILES  
!  The file is written using the collective write (MPI-2 standard)

  use m_openmpi
  use m_parameters
  use m_io
  use m_fields
  use m_work
  use m_fftw_pencil
  use m_timing

  implicit none

  character :: sym1
  integer :: n, nums_out, i, j, k

  integer(kind=MPI_INTEGER_KIND) :: fh
  integer(kind=MPI_OFFSET_KIND)  :: offset, offset_shift
  real*8, allocatable :: sctmp8(:,:,:)

  integer*4 :: nx1, ny1, nz1, nums1, nles1

  if (itime.eq.last_dump) return

  ! how many scalars to write
  nums_out = 0
  if (int_scalars) nums_out =  n_scalars

  ! using wrk array
  wrk(:,:,:,1:3+nums_out) = fields(:,:,:,1:3+nums_out)

  ! --------------- writing process ------------------

  fname = run_name//'.64.'//file_ext

  ! opening the file
  call MPI_INFO_CREATE(mpi_info, mpi_err)
  call MPI_FILE_OPEN(MPI_COMM_TASK,fname,MPI_MODE_WRONLY+MPI_MODE_CREATE,mpi_info,fh,mpi_err)

  ! Starting the stopwatch on the restart write
  call system_clock(timing0, dcpu)

  ! the master node writes the header with parameters
  if (myid.eq.0) then

     nx1 = nx;  ny1 = ny_all;  nz1 = nz_all;  nums1 = nums_out;
     nles1 = 0;
     count = 1
     call MPI_FILE_WRITE(fh,   nx1, count, MPI_INTEGER4, mpi_status, mpi_err)
     call MPI_FILE_WRITE(fh,   ny1, count, MPI_INTEGER4, mpi_status, mpi_err)
     call MPI_FILE_WRITE(fh,   nz1, count, MPI_INTEGER4, mpi_status, mpi_err)
     call MPI_FILE_WRITE(fh, nums1, count, MPI_INTEGER4, mpi_status, mpi_err)
     call MPI_FILE_WRITE(fh, nles1, count, MPI_INTEGER4, mpi_status, mpi_err)
     call MPI_FILE_WRITE(fh,  TIME, count, MPI_REAL8, mpi_status, mpi_err)
     call MPI_FILE_WRITE(fh,    DT, count, MPI_REAL8, mpi_status, mpi_err)
  end if

  ! all nodes write their stuff into the file
  count = nx * ny
  offset_shift = nx**2*8
  writing_fields: do n = 1, 3 + nums_out
     offset = 36 + my_col*nx*ny*8 + my_row*nx*nx*nz*8 + (n-1)*nx**3*8
     do k = 1, nz
        call MPI_FILE_WRITE_AT(fh, offset, fields(1,1,k,n), count, MPI_REAL8, mpi_status, mpi_err)
        offset = offset + offset_shift
     end do
  end do writing_fields


  call MPI_FILE_CLOSE(fh, mpi_err)
  call MPI_INFO_FREE(mpi_info, mpi_err)

  ! Stopping the stopwatch on the restart time
  call system_clock(timing1, dcpu)
  timing2 = (timing1-timing0) / (60 * dcpu) + 1
  count = 1
  call MPI_ALLREDUCE(timing2, restart_write_duration, count, MPI_INTEGER8, MPI_MAX, &
       MPI_COMM_TASK, mpi_err)
  sym1 = 's'
  if (restart_write_duration.eq.1) sym1 = ' '


  if (verbose) then
     write(out,*) '------------------------------------------------'
     write(out,*) 'Restart file written (par): '//trim(fname)
     if (int_scalars) then
        write(out,"(' Velocities and ',i3,' passive scalars')") nums_out
     else
        write(out,"(' (velocities only)')")
     end if
     write(out,"(' Restart file time = ',f15.10,i7)") time, itime
     write(out,"(' Writing took ',i3,' minute',a)") restart_write_duration, sym1
     write(out,*) '------------------------------------------------'
     call flush(out)
  end if
  last_dump = ITIME

  return
end subroutine restart_write

!================================================================================
!================================================================================
!================================================================================
!================================================================================

subroutine restart_read

  use m_openmpi
  use m_parameters
  use m_io
  use m_fields
  use m_work
  use m_fftw_pencil
  implicit none

  integer*4    :: nx1,ny1,nz1, nums1, nles1, nums_read
  integer      :: i, j, k, n

  integer(kind=MPI_INTEGER_KIND) :: fh
  integer(kind=MPI_OFFSET_KIND)  :: offset, offset_shift

  ! checking if the restart file exists
  fname = run_name//'.64.'//file_ext
  inquire(file=fname,exist=there)
  if(.not.there) then
     if (verbose) write(out,*) '*** error: Cannot find file : '//trim(fname)
     stop
  end if

  if (verbose) write(out,*) 'Reading from the file: ',trim(fname)
  if (verbose) call flush(out)

  ! ----------------------------------------------------------------------
  ! first reading the parameters from the restart file.
  ! the root process opens it and reads the parameters, then broadcasts 
  ! the parameters.  After that it's decided if the parameters make sense,
  ! how many scalars to read etc.
  ! ----------------------------------------------------------------------

  if (myid.eq.0) then
     open(91,file=fname,form='unformatted',access='stream')
     read(91) nx1, ny1, nz1, nums1, nles1, TIME, DT
     close(91)
  end if

  call MPI_BCAST(nx1,  1,MPI_INTEGER4,0,MPI_COMM_TASK,mpi_err)
  call MPI_BCAST(ny1,  1,MPI_INTEGER4,0,MPI_COMM_TASK,mpi_err)
  call MPI_BCAST(nz1,  1,MPI_INTEGER4,0,MPI_COMM_TASK,mpi_err)
  call MPI_BCAST(nums1,1,MPI_INTEGER4,0,MPI_COMM_TASK,mpi_err)
  call MPI_BCAST(nles1,1,MPI_INTEGER4,0,MPI_COMM_TASK,mpi_err)

  call MPI_BCAST(TIME,1,MPI_REAL8,0,MPI_COMM_TASK,mpi_err)
  call MPI_BCAST(  DT,1,MPI_REAL8,0,MPI_COMM_TASK,mpi_err)

  ! checking if the array sizes are the same in .in file and restart file
  if (nx.ne.nx1 .or. ny_all.ne.ny1 .or. nz_all.ne.nz1) then
     if (verbose) write(out,*) '*** error: Dimensions are different'
     if (verbose) write(out,*) '***     .in file: ',nx,ny_all,nz_all
     if (verbose) write(out,*) '*** restart file: ',nx1,ny1,nz1
     if (verbose) call flush(out)
     call my_exit(-1)
  end if

!-----------------------------------------------------------------------
!     dealing with scalars.
!     
!     The number of scalars can be varied throughout the simulation.
!     If the restart file has fewer scalars than the 
!     .in file, the scalars are added and initialized according to
!     their description in the .in-file.  
!     If the restart file has more scalars than the .in file, the
!     extra scalars are dropped.
!     
!     in short, whatever is specfied in the .in file, prevails.
!-----------------------------------------------------------------------

  if (n_scalars.lt.nums1) then

     if (verbose) write(out,*) ' WARNING: nums in restart file:',nums1
     if (verbose) write(out,*) '          nums in .in file    :',n_scalars
     if (verbose) write(out,'(''Losing '',i3,'' scalars.'')') nums1-n_scalars
     if (verbose) call flush(out)
     nums_read = n_scalars

  else if (n_scalars.gt.nums1) then

     if (verbose) write(out,*) ' WARNING: nums in restart file:',nums1
     if (verbose) write(out,*) '          nums in .in file    :',n_scalars
     if (verbose) write(out,'(''Adding '',i3,'' scalars.'')') n_scalars-nums1
     if (verbose) call flush(out)
     nums_read = nums1

     ! initializing the added scalars
     do n=nums1+1,n_scalars
!if (verbose) write(out,*) "This print signifies the initialization of a scalar ", n
!if (verbose) write(out,*) " *** THE SCALAR IS NOT INITIALIZED, though..."
!call init_scalar(n)
     end do

  else

     nums_read = n_scalars

  end if

!----------------------------------------------------------------------
!  ------------ reading the stuff from the restart file ---------------
!----------------------------------------------------------------------

  ! opening the file
  call MPI_INFO_CREATE(mpi_info, mpi_err)
  call MPI_FILE_OPEN(MPI_COMM_TASK,fname,MPI_MODE_RDONLY,mpi_info,fh,mpi_err)

  ! note that the data from the restart file has dimensions (nx,ny,nz),
  ! while the fields array has fimensions (nx+2,ny,nz).  
  ! that is why we need to read each field in the sctmp array first, and then
  ! rearrange it and put into the fields array.

  count = nx * ny
  offset_shift = nx**2*8
  reading_fields: do n = 1, 3 + nums_read

     if (verbose) write(out,"('  Reading variable # ',i3)") n
     if (verbose) call flush(out)

     offset = 36 + my_col*nx*ny*8 + my_row*nx*nx*nz*8 + (n-1)*nx**3*8
     do k = 1, nz
        call MPI_FILE_READ_AT_ALL(fh, offset, fields(1,1,k,n), count, MPI_REAL8, mpi_status, mpi_err)
        offset = offset + offset_shift
     end do
  end do reading_fields

  call MPI_FILE_CLOSE(fh, mpi_err)
  call MPI_INFO_FREE(mpi_info, mpi_err)

  if (verbose) write(out,*) "Restart file successfully read."
  if (verbose) call flush(out)
  if (verbose) write(out,"(70('-'))")


  return
end subroutine restart_read
