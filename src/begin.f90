subroutine begin

  use m_parameters

  if(ITMIN.eq.0) then
     call begin_new
  else
     call begin_restart
  endif

  return
end subroutine begin

!================================================================================
!  This file contains two subroutines that set up the flow field:
!  BEGIN_NEW - for the case of new simulations
!  BEGIN_RESTART - for the restart case
!================================================================================
subroutine begin_new

  use m_openmpi
  use m_parameters
  use m_fields
  use m_statistics
  implicit none

  if (verbose) write(out,"(70('-'))") 
  if (verbose) write(out,*) '                    NEW SIMULATION '
  if (verbose) write(out,"(70('-'))") 
  if (verbose) call flush(out)

  ! defining time
  TIME = zip

  ! deciding if we advance scalars or not
  if (TSCALAR.le.zip .and. n_scalars.gt.0) then
     int_scalars = .true.
     call init_scalars
  end if

  ! defining the iteration number and file_ext
  ITIME = 0
  file_ext = '000000'

  ! initializing variables
  if (task.eq.'hydro') then
     call init_velocity
     call io_write_4
  end if

  if (task_split) then
     stop "BEGIN_NEW: task_split is true, have not implemented that yet"
     !call fields_to_stats
     !if (task.eq.'stats') call stat_main
  end if

  if (verbose) write(out,*) "Finished the subroutine begin."
  if (verbose) call flush(out)

  return
end subroutine begin_new

!================================================================================
!================================================================================

subroutine begin_restart

  use m_parameters
  use m_io
  use m_fields
  use m_timing

  implicit none

  integer :: i

!--------------------------------------------------------------------------------
  if (verbose) write(out,"(70('-'))") 
  if (verbose) write(out,*) '                           RESTART '
  if (verbose) write(out,"(70('-'))") 
  if (verbose) call flush(out)

  ITIME = ITMIN
  call get_file_ext

  ! reading the restart file
  if (task.eq.'hydro') call restart_read

  ! if the scalars are present and time is greater than TSCALAR,
  ! define int_scalars as true
  if (n_scalars.gt.0 .and. time.ge.TSCALAR) int_scalars = .true.

  ! redefining the timestep.
  ! the code uses Adams-Bashforth time-stepping scheme,
  ! which is 2nd order accurate in time.  After the restart, the first
  ! timestep is done using a simple Euler scheme (1st order in time).
  ! To help maintain any sensible accuracy, we need to start with
  ! the timestep which is smaller than the last.
  if (variable_dt .and. NSCHEME.eq.0) then
     if (verbose) write(out,*) "Making the timestep smaller: ",dt
     if (verbose) call flush(out)
     dt = half * dt
    if (verbose) write(out,"(70('-'))")
  end if


!!$!================================================================================
!!$!  Checking the parallel read speed (reading 100 times)
!!$!  Currently the speedup factor from using parallel read is about 2.5
!!$!================================================================================
!!$  if(task.eq.'hydro') then
!!$     call m_timing_check
!!$     if (verbose) write(out,*) 'Start! ',cpu_min,cpu_sec
!!$     do i = 1,200
!!$        call restart_read! _parallel
!!$     end do
!!$     call m_timing_check
!!$     if (verbose) write(out,*) 'Finish!',cpu_min,cpu_sec
!!$
!!$  end if
!!$  call MPI_BARRIER(MPI_COMM_WORLD, mpi_err)
!!$  stop 'checking the restart read speed'
!!$
!!$!================================================================================
!!$!  Checking the accuracy of the parallel read 
!!$!  (reading parallel first, then serial and comparing)
!!$!================================================================================
!  if (task.eq.'hydro') then
!
!     call restart_read_parallel
!
!     allocate(zhopa(nx+2,ny,nz,3+n_scalars), stat=ierr)
!     if (ierr.ne.0) stop 'cannot allocate zhopa'
!     zhopa = zip
!     zhopa = fields
!
!     call restart_read
!
!     print "(10e15.6)",maxval(abs(zhopa-fields))
!  end if
!  call MPI_BARRIER(MPI_COMM_WORLD, mpi_err)
!  stop 'checking the restart read'
!!$!================================================================================

!!$  ! deciding whether we advance scalars or not
!!$  !  NOTE: This is done in the rhs_scalars now
!!$  if (n_scalars.gt.0  .and. time.gt.TSCALAR) then
!!$     int_scalars = .true.
!!$     if (verbose) write(out,"('Advancing ',i3,' scalars.')") n_scalars
!!$  end if

!!$  if (task.eq.'parts') call particles_init

  return
end subroutine begin_restart
