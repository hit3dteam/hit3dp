module m_timestep

  use m_parameters
  use m_work
  use m_fields
  use m_forcing

  logical :: first_order_velocity = .true., first_order_scalars = .true.

  ! arrays for Adams-Bashforth scheme
  real (kind=8), allocatable :: rhs_old(:,:,:,:)

  ! the variable that indicates the RK stage
  ! must initialize to 1 becuase it's used in AB iteration as well (see rhs_scalars)
  integer :: rk_stage = 1

  ! indicies that are used by our RK4 iterations
  integer :: nrs1, nrs2, n0v1, n0v2, n0s1, n0s2,  nnv1, nnv2, nns1, nns2

  ! output indicator for the timestep limiter
  character :: dt_info=' '

contains

!================================================================================
!================================================================================
  subroutine m_timestep_init

    implicit none

    if (.not.allocated(wrk)) then
       if (verbose) write(out,"('*** M_TIMESTEP_INIT: wrk not allocated!')")
       if (verbose) call flush(out)
       call my_exit(-1)
    end if


    select case (NSCHEME)

    case (0) 
       ! Adams-Bashforth
       ! array for the RHS from previous timestep
       if (task.eq.'hydro') then
          allocate(rhs_old(nx,ny,nz,3+n_scalars)) 
          rhs_old = zip
       end if

    case (4)
       ! Runge-Kutta 4th order

       ! the arrays that are needed are part of the work array
       ! now we have to define the following indicies.  Their meaning is:
       ! nrs1:nrs2 - RHS for scalars that are in the array fields
       ! n0v1:n0v2 - velocities_old
       ! n0s1:n0s2 - scalars_old
       ! nnv1:nnv2 - velocities_new
       ! nns1:nns2 - scalars_new

       nrs1 = 3 + 1
       nrs2 = 3 + n_scalars

       n0v1 = nrs2 + 4
       n0v2 = nrs2 + 6

       n0s1 = n0v2 + 1
       n0s2 = n0v2 + n_scalars

       nnv1 = n0s2 + 1
       nnv2 = n0s2 + 3

       nns1 = nnv2 + 1
       nns2 = nnv2 + n_scalars

       if (verbose) write(out,"('Indicies: ',10i3)") nrs1, nrs2, n0v1, n0v2, &
            n0s1, n0s2, nnv1, nnv2, nns1, nns2
       if (verbose) call flush(out)

    case default
       if (verbose) write(out,"('*** M_TIMESTEP_INIT: invalid NSCHEME:',i2)") NSCHEME
       if (verbose) call flush(out)
       call my_exit(-1)
    end select

    return
  end subroutine m_timestep_init

!================================================================================
!  Timestep subroutine
!================================================================================

  subroutine timestep

    implicit none

    ! First check if the time is right to start computing scalar fields
    if (n_scalars .gt.0  .and. .not. int_scalars  .and. time .ge. TSCALAR) then
       int_scalars = .true.
       call init_scalars
       if (verbose) write(out,"('Advancing ',i3,' scalars.')") n_scalars
       if (verbose) call flush(out)
       call io_write_4
    end if

    select case (NSCHEME)
    case (0)
       ! Adams-Bashforth, 2nd order in time
       call timestep_ab

    case(4)
       ! Runge-Kutta 4th order
       call timestep_rk4
    end select
  end subroutine timestep


!================================================================================
!  Adams-Bashforth: timestepping 
!================================================================================
  subroutine timestep_ab

    implicit none
    integer :: n

    if (int_scalars) then
       ! calculate the RHS for the scalars
       call rhs_scalars
       ! advance scalars - either Euler or Adams-Bashforth
       n = 3 + n_scalars
       if (first_order_scalars) then
          rhs_old(:,:,:,4:n) = wrk(:,:,:,4:n)
          fields(:,:,:,4:n) = fields(:,:,:,4:n) + dt * rhs_old(:,:,:,4:n)
          first_order_scalars = .false.
       else
          fields(:,:,:,4:n) = fields(:,:,:,4:n) + &
               dt * ( 1.5d0 * wrk(:,:,:,4:n)  - 0.5d0 * rhs_old(:,:,:,4:n) )
          rhs_old(:,:,:,4:n) = wrk(:,:,:,4:n)
       end if
    end if

    ! now perform timestep for velocity
    call rhs_velocity
    call force_velocity
    if (first_order_velocity) then
       rhs_old(:,:,:,1:3) = wrk(:,:,:,1:3)
       fields(:,:,:,1:3) = fields(:,:,:,1:3) + dt * rhs_old(:,:,:,1:3)
       first_order_velocity = .false.
    else
       fields(:,:,:,1:3) = fields(:,:,:,1:3) + &
            dt * ( 1.5d0 * wrk(:,:,:,1:3)  - 0.5d0 * rhs_old(:,:,:,1:3) )
       rhs_old(:,:,:,1:3) = wrk(:,:,:,1:3)
    end if
    call pressure

    return
  end subroutine timestep_ab


!================================================================================
!  RK4: timestepping for scalars and velocity at the same time
!================================================================================
  subroutine timestep_rk4
    implicit none

    ! first, save the current values of velocity and scalars to "_0" variables
    wrk(:,:,:,n0v1:n0v2) = fields(:,:,:,1:3)
    if (int_scalars) wrk(:,:,:,n0s1:n0s2) = fields(:,:,:,nrs1:nrs2)

    ! --------------------------------------------------
    ! STAGE 1
    ! --------------------------------------------------
    rk_stage = 1
    if (int_scalars) then
       ! Get the R1(s_old)
       call rhs_scalars
       ! s_new = s_old + dt/6*R1
       wrk(:,:,:,nns1:nns2) = wrk(:,:,:,n0s1:n0s2) + dt / 6.d0 * wrk(:,:,:,nrs1:nrs2)
       ! s* = s_old + dt/2 * R1
       fields(:,:,:,nrs1:nrs2) = wrk(:,:,:,n0s1:n0s2) + dt * 0.5d0 * wrk(:,:,:,nrs1:nrs2)
    end if
    ! get R1(u_old)
    call rhs_velocity
    call force_velocity
    ! u_new = u_old + dt/6*R1
    wrk(:,:,:,nnv1:nnv2) = wrk(:,:,:,n0v1:n0v2) + dt / 6.d0 * wrk(:,:,:,1:3)
    ! u* = u_old + dt/2 * R1
    fields(:,:,:,1:3) = wrk(:,:,:,n0v1:n0v2) + dt * 0.5d0 * wrk(:,:,:,1:3)
    call pressure


    ! --------------------------------------------------
    ! STAGE 2
    ! --------------------------------------------------
    rk_stage = 2
    if (int_scalars) then
       ! get R2(s*)
       call rhs_scalars
       ! s_new = s_old + dt/6*R1 + dt/3*R2
       wrk(:,:,:,nns1:nns2) = wrk(:,:,:,nns1:nns2) + dt / 3.d0 * wrk(:,:,:,nrs1:nrs2)
       ! s* = s_old + dt/2 * R2
       fields(:,:,:,nrs1:nrs2) = wrk(:,:,:,n0s1:n0s2) + dt * 0.5d0 * wrk(:,:,:,nrs1:nrs2)
    end if
    ! get R2(u*)
    call rhs_velocity
    call force_velocity
    ! u_new = u_old + dt/6*R1 + dt/3*R2
    wrk(:,:,:,nnv1:nnv2) = wrk(:,:,:,nnv1:nnv2) + dt / 3.d0 * wrk(:,:,:,1:3)
    ! u* = u_old + dt/2 * R2
    fields(:,:,:,1:3) = wrk(:,:,:,n0v1:n0v2) + dt * 0.5d0 * wrk(:,:,:,1:3)
    call pressure


    ! --------------------------------------------------
    ! STAGE 3
    ! --------------------------------------------------
    rk_stage = 3
    if (int_scalars) then
       ! get R3(s*)
       call rhs_scalars
       ! s_new = s_old + dt/6*R1 + dt/3*R2 + dt/3*R3
       wrk(:,:,:,nns1:nns2) = wrk(:,:,:,nns1:nns2) + dt / 3.d0 * wrk(:,:,:,nrs1:nrs2)
       ! s* = s_old + dt * R3
       fields(:,:,:,nrs1:nrs2) = wrk(:,:,:,n0s1:n0s2) + dt * wrk(:,:,:,nrs1:nrs2)
    end if
    ! get R3(u*)
    call rhs_velocity
    call force_velocity
    ! u_new = u_old + dt/6*R1 + dt/3*R2 + dt/3*R3
    wrk(:,:,:,nnv1:nnv2) = wrk(:,:,:,nnv1:nnv2) + dt / 3.d0 * wrk(:,:,:,1:3)
    ! u* = u_old + dt * R3
    fields(:,:,:,1:3) = wrk(:,:,:,n0v1:n0v2) + dt * wrk(:,:,:,1:3)
    call pressure


    ! --------------------------------------------------
    ! STAGE 4
    ! --------------------------------------------------
    rk_stage = 4
    if (int_scalars) then
       call rhs_scalars
       ! s_new = s_old + dt/6*R1 + dt/3*R2 + dt/3*R3 + dt/6*R4
       fields(:,:,:,nrs1:nrs2) = wrk(:,:,:,nns1:nns2) + dt / 6.d0 * wrk(:,:,:,nrs1:nrs2)
    end if
    ! get R4(u*)
    call rhs_velocity
    call force_velocity
    ! u_new = u_old + dt/6*R1 + dt/3*R2 + dt/3*R3 + dt/6*R4
    fields(:,:,:,1:3) = wrk(:,:,:,nnv1:nnv2) + dt / 6.d0 * wrk(:,:,:,1:3)
    call pressure


    return
  end subroutine timestep_rk4


!================================================================================
!  Calculate the new timestep in case of variable timestep
!================================================================================
  subroutine timestep_get_new_dt

    use m_openmpi
    use m_parameters
    implicit none

    integer :: n
    real*8  :: courant_max_ab=0.1
    real*8  :: dt_conv, dt_diff, dt_scal
    real*8  :: eff_kmax

    select case (NSCHEME)

    case(0)
       ! Adams-Bashforth
       ! use the Courant number is calculated every iteration in rhs_velocity.f90
       if (courant.lt.courant_max_ab) then
          DT = DT * 1.01d0
          DT = min(DT,0.01)
       else
          DT = DT/1.05d0
       end if
       ! make sure DT is appropriate for scalars
       ! DT < 0.09 * dx^2*Pe
       if (int_scalars) then
          do n=1,n_scalars
             DT = min(DT,DT*courant_max_ab/courant*sc(n))
          end do
       end if

    case(4)
       ! Runge-Kutta 4th order
       ! if dealias=1, then the max wavenumber in the code is 0.687*nx
       ! this happens because we do not de-alias spherically but rather
       ! use the array ialias.
       eff_kmax = 0.687 * real(nx,8)
       ! if dealias=0 (2/3 rule), then the max wavenumber in the code is nx/sqrt(3)
       if (dealias.eq.0) eff_kmax = real(nx,8) / sqrt(3.d0)

       ! convective limit, defined by velmax
       ! dt_conv = 2.8d0 / velmax / eff_kmax
       dt_conv = 2.7d0 / velmax / eff_kmax
       ! diffusion limit, defined by viscosity
       ! dt_diff = 2.7d0 / nu / eff_kmax**2
       dt_diff = 2.4d0 / nu / eff_kmax**2
       ! diffusion limit, defined by Schmidt numbers of scalars
       dt_scal = 2.d0 * dt_diff
       if (int_scalars) dt_scal =  dt_diff * minval(SC(1:n_scalars))

       ! choosing the smallest dt out of three
       dt = min(dt_conv, dt_diff, dt_scal)

       if (dt.eq.dt_conv) dt_info = 'c'
       if (dt.eq.dt_diff) dt_info = 'd'
       if (dt.eq.dt_scal) dt_info = 's'

       courant = velmax * dt / dx

!if (verbose) write(out,"(20e15.6)") dt, dt_conv, dt_diff, dt_scal


    case default
       if (verbose) write(out,*) "TIMESTEP_GE_NEW_DT: invalid NSCHEME:",NSCHEME
       call my_exit(-1)
    end select

    return
  end subroutine timestep_get_new_dt

!================================================================================
!  Get the initial timestep, when the velmax is not availabe
!  This program is called at the start or restart, when the values of
!  courant (for the case of AB) or velmax (for other cases) have not been
!  computed yet.  It calculates them, then calls timestep_get_new_dt
!================================================================================
  subroutine timestep_get_initial_dt

    use m_openmpi
    use m_parameters
    use m_fftw_pencil
    implicit none

    integer :: n
    real*8  :: courant_max_ab=0.1
    real*8  :: dt_conv, dt_diff, dt_scal

    ! first get the velocities in x-space
    wrk(:,:,:,1:3) = fields(:,:,:,1:3)
    ! performing IFFT to convert them to the X-space
    call FFTW3D(-1,1)
    call FFTW3D(-1,2)
    call FFTW3D(-1,3)

    ! now get the values of courant or velmax
    select case (NSCHEME)
    case(0)
       if (mod(itime,5).eq.0) then
          ! in case of AB timestepping, getting the Courant number 
          ! (on the master process only)
          wrk(:,:,:,4) = abs(wrk(:,:,:,1)) + abs(wrk(:,:,:,2)) + abs(wrk(:,:,:,3))
          rtmp = maxval(wrk(1:nx,:,:,4))
          call MPI_REDUCE(rtmp,courant,1,MPI_REAL8,MPI_MAX,0,MPI_COMM_TASK,mpi_err)
          if (variable_dt) then
             count = 1
             call MPI_BCAST(courant,count,MPI_REAL8,0,MPI_COMM_TASK,mpi_err)
          end if
          courant = courant * dt / dx
       end if
    case default
       ! other methods
       ! in this case, just getting the max velocity and sending it to the 
       ! master process
       wrk(:,:,:,4) = wrk(:,:,:,1)**2 + wrk(:,:,:,2)**2 + wrk(:,:,:,3)**2
       rtmp =  sqrt(maxval(wrk(1:nx,:,:,4)))
       count = 1
       call MPI_REDUCE(rtmp, velmax, count, MPI_REAL8, MPI_MAX, 0, MPI_COMM_TASK, mpi_err)
       if (variable_dt) call MPI_BCAST(velmax, count, MPI_REAL8, 0, MPI_COMM_TASK, mpi_err)
    end select

    ! now get the timestep
    call timestep_get_new_dt

    return
  end subroutine timestep_get_initial_dt

end module m_timestep
