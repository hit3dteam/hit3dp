!================================================================================
!================================================================================

  subroutine write_tmp4

    use m_openmpi
    use m_io
    use m_parameters
    use m_work
    implicit none

    integer :: my_out=13, i, j, k
    integer(kind=MPI_INTEGER_KIND) :: sizes(3), fh
    integer(kind=MPI_OFFSET_KIND)  :: offset, offset_shift

!!$    print *,'MPI_INTEGER_KIND = ',MPI_INTEGER_KIND
!!$    print *,'MPI_OFFSET_KIND = ',MPI_OFFSET_KIND
!!$    return


    ! --- defining the size of whole array
    sizes(1)=nx;   sizes(2)=nx;   sizes(3)=nx

    ! --- writing into the file with appropriate offset
    call MPI_INFO_CREATE(mpi_info,mpi_err)

!write(out,*) 'created info:', mpi_err
!call flush(out)

    if(mpi_err.ne.0) stop '*** WRITE_TMP4_ALL: cannot create mpi_info'

    call MPI_FILE_OPEN(MPI_COMM_TASK,fname,MPI_MODE_WRONLY+MPI_MODE_CREATE,mpi_info,fh,mpi_err)
    if(mpi_err.ne.0) stop '*** WRITE_TMP4_ALL: cannot open file'

!write(out,*) 'opened file:', mpi_err
!call flush(out)

    offset = 0
    if (myid.eq.0) call MPI_FILE_WRITE_AT(fh,offset,sizes,3,MPI_INTEGER4,mpi_status,mpi_err)

    count = nx * ny
    offset = 12 + 4*(my_row*nx*nx*nz + my_col*nx*ny)
    offset_shift = 4*(nx * ny * pencils_y)
!    print *,'ready:', myid, my_row, my_col, count, offset, offset_shift

    do k = 1, nz
!       print *,'writing:', myid, k, offset

!write(out,*) 'writing: ', k
!call flush(out)

!       call MPI_FILE_WRITE_AT_ALL(fh,offset,tmp4(1,1,k),count,MPI_REAL4,mpi_status,mpi_err)
       call MPI_FILE_WRITE_AT(fh,offset,tmp4(1,1,k),count,MPI_REAL4,mpi_status,mpi_err)
       offset = offset + offset_shift
    end do

    call MPI_FILE_CLOSE(fh, mpi_err)
    call MPI_INFO_FREE(mpi_info, mpi_err)

    return
  end subroutine write_tmp4
