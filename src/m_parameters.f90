!================================================================================
! M_PARAMETERS - module for all parameters in the calculation: 
!                such as array dimensions, reynolds numbers, switches/flags etc.
!
! Time-stamp: <2018-10-11 14:25:22 (chs1pal)>
! Time-stamp: <2008-11-20 17:27:59 MST (vladimirova)>
!================================================================================

module m_parameters

  use m_openmpi
  use m_io
  implicit none

  ! --- problem related

  character*10 :: run_name

  ! --- input file arameters 
  integer :: nx, ny, nz, ny_all, nz_all ! Dimensions of the problem
  integer :: nxyz, nxyz_all
  integer :: n_scalars        ! # of scalars
  real*8  :: time   ! time of simulation
  real*8  :: dx, dy, dz
  integer :: kmax
  
  integer :: pencils_y, pencils_z
  
  logical :: benchmarking=.false.

  integer :: ITIME, ITMIN, ITMAX, IPRINT, IWRITE8, IWRITE4

  real*8  :: TMAX, TRESCALE, TSCALAR, RE, nu, dt

  ! the timestepping scheme
  integer :: NSCHEME

  ! now many times to rescale teh velocities
  integer :: NRESCALE

  integer :: flow_type

  logical :: variable_dt

  integer :: isp_type, ir_exp, force_type

  real*8  :: peak_wavenum

  real*8 :: force_mag

  integer*4 :: kfmin, kfmax ! Min and max wavenumbers for forcing in a band (integers)

  real*8 :: courant, velmax


  integer  :: dealias

  integer :: det_rand
  real*8  :: RN1, RN2, RN3

  ! particle-related 

  ! indicator that says which particle tracking scheme to use:
  ! 0 = trilinear
  ! 1 = spectral
  ! 2 = tricubic
  ! trilinear by default
  integer :: particles_tracking_scheme = 0
  real*8  :: starttime_particles
  
  ! sometimes we want to advect particles by locally averaged field
  ! the following variables address that concern
  real*8  :: particles_filter_size

  ! number of particles assigned to the processor
  ! and the total number of particles
  integer(kind=MPI_INTEGER_KIND) :: np, np1, nptot

  ! If using Large Eddy Simulation (LES), the LES model ID is here
  integer :: les_model

  integer, allocatable :: scalar_type(:)
  real*8, allocatable  :: pe(:), sc(:), ir_exp_sc(:), peak_wavenum_sc(:), reac_sc(:)


  ! constants
  real*8  :: zip=0.0d0, half=0.5d0
  real*8  :: one=1.0d0,two=2.0d0,three=3.d0,four=4.d0,five=5.d0, six=6.d0


  integer :: last_dump

  ! --- supporting stuff
  logical      :: there
  logical      :: fos, fov
  integer      :: ierr
  real*8       :: PI, TWO_PI

  logical      :: int_scalars, int_particles

  ! --- number of LES variables in the arrays (initialized to zero)
  integer :: n_les = 0

!================================================================================
contains
!================================================================================

  subroutine m_parameters_init

    implicit none

    call get_run_name

    ! constants
    PI     = four * atan(one)
    TWO_PI = two * PI

    ! switches
    int_scalars = .false.

    call read_input_file

    ! maximum resolved wavenumber
    if (dealias.eq.0) then
       kmax = nx/3
    elseif (dealias.eq.1) then
       kmax = floor(real(nx,8) / three * sqrt(two))
    else
       if (verbose) write(out,*) "*** M_PARAMETERS_INIT: wrong dealias flag: ",dealias
       if (verbose) call flush(out)
       call my_exit(-1)
    end if
    

    if (verbose) write(out,*) "kmax = ",kmax
    if (verbose) call flush(out)

  end subroutine m_parameters_init

!================================================================================

  subroutine get_run_name
    implicit none

    character*80 :: tmp_str
    integer      :: iargc

    ! reading the run_name from the command line
    if(iargc().eq.0) then
       call getarg(0,tmp_str)
       if (verbose) write(out,*) 'Format: ',trim(tmp_str),' <run name>'
       write(*,*)      'Format: ',trim(tmp_str),' <run name>'
       if (verbose) call flush(out)
       call MPI_FINALIZE(ierr)
       stop
    end if
    call getarg(1,run_name)
    if(len_trim(run_name).ne.10) then
       if (verbose) write(out,*) 'Run name: "',run_name,'"'
       if (verbose) write(out,*) '          "1234567890"'
       if (verbose) write(out,*) 'Length of run name is less than 10, sorry.'
       call MPI_FINALIZE(ierr)
       stop
    end if
    if (verbose) write(out,*) 'Run name: "',run_name,'"'
    if (verbose) call flush(out)

  end subroutine get_run_name


!================================================================================

  subroutine read_input_file

    implicit none

    logical :: there
    integer :: m, n
    integer*4  :: passed, passed_all
    character*80 :: str_tmp
    character*5 :: n_txt
    integer :: iargc

    ! making sure the input file is there
    inquire(file=run_name//'.in', exist=there)
    if(.not.there) then
       if (verbose) write(out,*) '*** cannot find the input file'
       if (verbose) call flush(out)
       call my_exit(-1)
    end if

    ! now the variable "passed" will show if the parameters make sense
    passed = 1


    ! -------------------------------------------------
    ! reading parameters from the input file
    ! and checking them for consistency
    ! -------------------------------------------------
    open(in,file=run_name//'.in',form='formatted')
    read(in,*) 
    read(in,*) 
    read(in,*)

    read(in,*,ERR=9000) nx
    read(in,*)

    ny_all = nx
    nz_all = nx

!--------------------------------------------------------------------------------
!  Splitting domain into pencils.
!
!  All that is required at this point is finding ny and nz, the rest is taken
!  care of in m_fftw.f90.
!
!  NOTE:  NZ MUST BE EVEN.  This is a requirement that cannot be changed at this
!  point, due to the way our implementation of FFTW stores things in memory.
!
!--------------------------------------------------------------------------------
!

! NOTE THAT THIS IMPLEMENTATION HAS RESTRICTIONS
! for the implementation, NX must be divisible by pencils_y and pencils_z 
! without a remainder.  Otherwise it messes up the data exchange during FFT
! THUS if NX=2^nm then number of processors also must be 2^k

   n = floor(sqrt(real(numprocs)))
!   n = min(nx/2, numprocs)
   if (iargc().eq.3) then
     call getarg(3,n_txt)
     read(n_txt,*) n
   end if
   do while (&
        mod(numprocs,n).ne.0 .or. &   ! n divides numprocs
        mod(nx,2*n)    .ne.0)         ! n divides nx and their ratio is even
      ! (the latter is needed to make the pencils even in z-direction)
      if (n.gt.nx/2) stop "Cannot split the given number of processors"
      n = n + 1
   end do
   m = numprocs / n
   ny = nx / m
   nz = nx / n
   pencils_y = m
   pencils_z = n

   my_row = myid / m     ! pencil's number in z (0 to ...)
   my_col = mod(myid,m)  ! pencil's number in y (0 to ...)

!--------------------------------------------------------------------------------

    if (verbose) write(out,"(70('-'))") 
    if (verbose) write(out,"(' NX, NY_ALL, NZ_ALL', 3i4)") nx, ny_all, nz_all
    if (verbose) write(out,"(' NX, NY    , NZ    ', 3i4)") nx, ny, nz
    if (verbose) call flush(out)

    ! final check that the split makes sense
    if (mod(nx,ny).ne.0 .or. mod(nx,nz).ne.0 .or. nz.lt.2) then
       if (verbose) write (out,*) "ERROR: cannot split the domain"
       call my_exit(-1)
    end if
       

    dx = 2.0d0 * PI / dble(nx)
    dy = 2.0d0 * PI / dble(ny_all)
    dz = 2.0d0 * PI / dble(nz_all)

    ! -------------------------------------------------------------

    read(in,*,ERR=9000,END=9000) ITMIN
    if (verbose) write(out,*) 'ITMIN =   ',ITMIN
    last_dump = ITMIN

    read(in,*,ERR=9000,END=9000) ITMAX
    if (verbose) write(out,*) 'ITMAX =   ',ITMAX

    read(in,*,ERR=9000,END=9000) IPRINT
    if (verbose) write(out,*) 'IPRINT=   ',IPRINT

    read(in,*,ERR=9000,END=9000) IWRITE8
    if (verbose) write(out,*) 'IWRITE8=   ',IWRITE8

    read(in,*,ERR=9000,END=9000) IWRITE4
    if (verbose) write(out,*) 'IWRITE4=  ',IWRITE4
    read(in,*)
    if (verbose) write(out,"(70('-'))")
    if (verbose) call flush(out)

    ! ------------------------------------------------------------

    read(in,*,ERR=9000,END=9000) TMAX
    if (verbose) write(out,*) 'TMAX     =',TMAX  

    read(in,*,ERR=8000,END=9000) TRESCALE, NRESCALE
100 if (verbose) write(out,*) 'TRESCALE, NRESCALE =',TRESCALE, NRESCALE

    read(in,*,ERR=9000,END=9000) TSCALAR
    if (verbose) write(out,*) 'TSCALAR  =',TSCALAR
    read(in,*)
    if (verbose) write(out,"(70('-'))")
    if (verbose) call flush(out)

!      if(TSCALAR.le.TRESCALE) then
!        TSCALAR = TRESCALE
!        if (verbose) write(out,*) '*** RESET: TSCALAR = ',TSCALAR
!      end if

    ! ------------------------------------------------------------

    read(in,*,ERR=9000,END=9000) flow_type
    if (verbose) write(out,*) 'flow_type    ', flow_type
    read(in,*)
    if (verbose) write(out,"(70('-'))")
    if (verbose) call flush(out)

    ! ------------------------------------------------------------

    read(in,*,ERR=9000,END=9000) RE
    if (verbose) write(out,*) 'RE    =   ',RE   

    nu = 1.0d0/RE

    read(in,*,ERR=9000,END=9000) DT
    if (verbose) write(out,*) 'DT    =   ',DT   
    if (dt.lt.0.0d0) then
       variable_dt = .false.
       dt = -dt
    else
       variable_dt = .true.
    end if

    read(in,*,ERR=9000,END=9000) NSCHEME
    if (verbose) write(out,*) 'NSCHEME=   ',NSCHEME


    read(in,*)
    if (verbose) write(out,"(70('-'))")
    if (verbose) call flush(out)

    ! ------------------------------------------------------------

    read(in,*,ERR=9000,END=9000) isp_type
    if (verbose) write(out,*) 'isp_type=   ', isp_type

    read(in,*,ERR=9000,END=9000) ir_exp
    if (verbose) write(out,*) 'ir_exp   =   ', ir_exp

    read(in,*,ERR=9000,END=9000) peak_wavenum
    if (verbose) write(out,*) 'peak_wavenum =   ',peak_wavenum
    read(in,*)
    if (verbose) write(out,"(70('-'))")
    if (verbose) call flush(out)

    ! ------------------------------------------------------------

    read(in,*,ERR=9000,END=9000) force_type
    if (verbose) write(out,*) 'force_type', force_type

    read(in,*,ERR=9000,END=9000) kfmin, kfmax
    if (kfmin.lt.1) then
       if (verbose) write(out,*) "KFMIN WAS SMALLER THAN 1, MAKING IT 1"
       if (verbose) call flush(out)
    end if
    kfmin = max(kfmin, 1)
    if (verbose) write(out,*) 'kfmin, kfmax:',kfmin, kfmax

    read(in,*,ERR=9000,END=9000) force_mag
    if (verbose) write(out,*) 'force_mag = ',force_mag

    read(in,*)
    if (verbose) write(out,"(70('-'))")
    if (verbose) call flush(out)

    ! -------------------------------------------------------------

    read(in,*,ERR=9000,END=9000) dealias
    if (verbose) write(out,*) 'dealias = ',dealias
    read(in,*)
    if (verbose) write(out,"(70('-'))")
    if (verbose) call flush(out)

    ! -------------------------------------------------------------

    read(in,*,ERR=9000,END=9000) det_rand
    if (verbose) write(out,*) 'det_rand =',det_rand

    read(in,*,ERR=9000,END=9000) RN1
    if (verbose) write(out,*) 'RN1      =',RN1

    read(in,*,ERR=9000,END=9000) RN2
    if (verbose) write(out,*) 'RN2      =',RN2

    read(in,*,ERR=9000,END=9000) RN3
    if (verbose) write(out,*) 'RN3      =',RN3
    read(in,*)
    if (verbose) write(out,"(70('-'))")
    if (verbose) call flush(out)

    ! -------------------------------------------------------------

    read(in,*,ERR=9000,END=9000) nptot
! DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG
    if (.not.task_split .and. nptot > 0) then
       if (verbose) write(out,*) "tasks are not split, making nptot=0"
       nptot = 0
    end if
! DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG

    if (verbose) write(out,*) 'nptot    =',nptot


    read(in,*,ERR=9000,END=9000) particles_tracking_scheme
    if (verbose) write(out,*) 'particles_tracking_scheme', particles_tracking_scheme

    select case (particles_tracking_scheme)
    case (0) 
       if (verbose) write(out,*) '--- Trilinear tracking'
    case (1)
       if (verbose) write(out,*) '--- CINT (cubic interpolation on integer nodes)'
    case (2)
       if (verbose) write(out,*) '--- Spectral tracking (CAUTION: SLOW!)'
    case default
       if (verbose) write(out,*) 'don''t recognize particle tracking:', &
                  particles_tracking_scheme
       if (verbose) write(out,*) 'reset to zero'
       particles_tracking_scheme = 0
    end select
    if (verbose) call flush(out)


! DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG
    if (particles_tracking_scheme .gt. 1) stop 'Cannot do this particle tracking'
! DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG



    read(in,*,ERR=9000,END=9000) starttime_particles
    if (verbose) write(out,*) 'starttime_particles: ',starttime_particles

    read(in,*,ERR=9000,END=9000) particles_filter_size
    if (verbose) write(out,*) 'particles_filter_size:',particles_filter_size

    if (particles_filter_size .gt. zip .and. particles_filter_size .lt. three*dx) then
       if (verbose) write(out,*) "particles_filter_size is too small (less than 3*dx)"
       if (verbose) write(out,*) particles_filter_size, three*dx
       if (verbose) call flush(out)
       call my_exit(-1)
    end if

    read(in,*)      


    ! -------------------------------------------------------------

    read(in,*,ERR=9000,END=9000) les_model
    if (verbose) write(out,*) 'les_model    =',les_model
    read(in,*)      
    if (verbose) write(out,"(70('-'))")
    if (verbose) call flush(out)

    ! This code does not know how to use LES yet.  So if les_model is
    ! nonzero, we stop
    if (les_model.ne.0) then
       if (verbose) write(out,*) "*** LES_MODEL IS NONZERO, STOPPING."
       if (verbose) call flush(out)
       call my_exit(-1)
    end if

    ! making sure that if the LES mode is on, the dealiasing is 3/2-rule
    if (les_model .gt. 0 .and. dealias .ne. 0) then
       dealias = 0
       if (verbose) write(out,*) "*** LES mode, changing dealias to 0."
       if (verbose) call flush(out)
    end if

    ! -------------------------------------------------------------

    read(in,*,ERR=9000,END=9000) n_scalars
    if (verbose) write(out,*) '# of scalars:', n_scalars
    read(in,*)
    if (verbose) write(out,"(70('-'))")
    if (verbose) call flush(out)

    ! ------------------------------------------------------------

    ! if there are scalars, then read them one by one
    if (n_scalars>0) then
       read(in,'(A)',ERR=9000,END=9000) str_tmp
       if (verbose) write(out,*) str_tmp
       if (verbose) call flush(out)

       ! reading parameters of each scalar
       allocate(scalar_type(n_scalars), pe(n_scalars), sc(n_scalars), &
            ir_exp_sc(n_scalars), peak_wavenum_sc(n_scalars), &
            reac_sc(n_scalars), stat=ierr)
       if (ierr.ne.0) passed = 0

       do n = 1,n_scalars
          read(in,*,ERR=9000,END=9000) scalar_type(n), sc(n), ir_exp_sc(n), &
               peak_wavenum_sc(n), reac_sc(n)
          if (verbose) write(out,'(9x,i4,1x,4(f8.3,1x))') scalar_type(n), sc(n), ir_exp_sc(n), &
               peak_wavenum_sc(n), reac_sc(n)
               if (verbose) call flush(out)

          PE(n) = nu/SC(n)       ! INVERSE Peclet number

       end do
    end if

    ! -------------------------------------------------------------

    ! closing the input file
    close(in)
    if (verbose) write(out,"(70('-'))") 
    if (verbose) call flush(out)

    ! defining the rest of the parameters

    nxyz = nx * ny * nz
    nxyz_all = nx * ny_all * nz_all

    ! ------------------------------------------------------------


!--------------------------------------------------------------------------------
!  Checking if the task splitting conflicts with particle advection.  Currently
!  we canot have split=never and have particles.  This is to be resolved later,
!  now my head is spinning already.
!--------------------------------------------------------------------------------
    if (.not.task_split .and. nptot.gt.0) then
       if (verbose) write(out,*) "*** READ_INPUT_FILE: Cannot have .not.task_split and nptot > 0.  Stopping"
       if (verbose) call flush(out)
       passed = 0
    end if
!--------------------------------------------------------------------------------


    count = 1
    call MPI_REDUCE(passed,passed_all,count,MPI_INTEGER4,MPI_MIN,0,MPI_COMM_WORLD,mpi_err)
    count = 1
    call MPI_BCAST(passed_all,count,MPI_INTEGER4,0,MPI_COMM_WORLD,mpi_err)

    if (passed.lt.one) then
       if (verbose) write(out,*) "not passed the check, stopping"
       if (verbose) call flush(out)
       stop
    end if

    return

!--------------------------------------------------------------------------------
!  ERROR PROCESSING
!--------------------------------------------------------------------------------

8000 continue
    NRESCALE = 0
    if (TRESCALE.gt.zip) NRESCALE = 1
    if (verbose) write(out,*) "*** NRESCALE IS AUTOMATICALLY ASSIGNED to be ONE"
    if (verbose) call flush(out)
    goto 100

9000 continue
    if (verbose) write(out,*)'An error was encountered while reading input file'
    if (verbose) call flush(out)
    stop
  end subroutine read_input_file

!================================================================================
!================================================================================
  subroutine get_file_ext
    implicit none

    write(file_ext,"(i6.6)") itime
    return
  end subroutine get_file_ext


!================================================================================
end module m_parameters
