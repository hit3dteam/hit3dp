!=================================================================================
module m_forcing
!=================================================================================

  use m_parameters, only : kfmax, force_mag, force_type


  integer*4 :: n_forced_nodes, n_forced_nodes_total

  ! coordinated of the forced nodes
  integer, allocatable :: ifn(:), jfn(:), kfn(:), k_shell(:)

!=================================================================================
contains
!=================================================================================

!================================================================================
!  Initialization of forcing mechanism
!================================================================================
  subroutine m_forcing_init

    use m_parameters
    use m_fftw_pencil
    implicit none
!--------------------------------------------------------------------------------
    integer :: i, j, k, n, n_shell
!--------------------------------------------------------------------------------
    ! if flow is not forced, return
    if (flow_type .ne. 1) return

    ! if this is not the "hydro" part of the code, return
    if (task.ne.'hydro') return
!--------------------------------------------------------------------------------
    select case (force_type)
    case (1)
       ! Machiels forcing (see article in PRL #79(18) p.3411)
       if (verbose) write(out,*) "Forcing #1: Machiels forcing - setting up"
       if (verbose) call flush(out)

       ! find out how many nodes are we forcing and book them
       n_forced_nodes = 0
       n_forced_nodes_total = 0
       do k = 1,nz
          do j = 1,ny
             do i = 1,nx
                n_shell = nint(sqrt(real(akx(k)**2 + aky(i)**2 + akz(j)**2, 4)))
                if (n_shell .ge. kfmin .and. n_shell .le. kfmax) then
                   n_forced_nodes = n_forced_nodes + 1
                end if
             end do
          end do
       end do

       ! reducing to the master process to find out the total number of forced nodes
       count = 1
       call MPI_REDUCE(n_forced_nodes, n_forced_nodes_total, count,  &
            MPI_INTEGER4, MPI_SUM, master, MPI_COMM_TASK, mpi_err)

       ! writing out the # of forced nodes
       if (verbose) write(out,*) 'Number of forced nodes for this process:',n_forced_nodes
       if (verbose .and. iammaster) write(out,*) ' total number of forced nodes:',n_forced_nodes_total
       if (verbose) call flush(out)

       ! allocating arrays for the coordinates of the forced nodes
       allocate(ifn(n_forced_nodes), jfn(n_forced_nodes), kfn(n_forced_nodes), &
            k_shell(n_forced_nodes))

       ! filling up the arrays
       n = 0
       do k = 1,nz
          do j = 1,ny
             do i = 1,nx
                n_shell = nint(sqrt(real(akx(k)**2 + aky(i)**2 + akz(j)**2, 4)))
                if (n_shell .ge. kfmin .and. n_shell .le. kfmax) then
                   n = n + 1
                   ifn(n) = i
                   jfn(n) = j
                   kfn(n) = k
                   k_shell(n) = n_shell
                end if
             end do
          end do
       end do

    case default
       if (verbose) write(out,*) 'WRONG FORCE TYPE:',force_type
       if (verbose) write(out,*) 'STOPPING'
       if (verbose) call flush(out)
       stop
    end select

    return
  end subroutine m_forcing_init

!================================================================================
!  adding forcing to the arrays wrk(:,:,:,1:3) that already contain the RHS for 
!  velocities
!================================================================================
  subroutine force_velocity

    use m_openmpi
    use m_io
    use m_parameters
    use m_fields
    use m_work
    use m_fftw_pencil
    use m_statistics

    implicit none
!--------------------------------------------------------------------------------
    integer :: i, j, k, n_shell, n
    real*8 :: fac, fac2, sc_rad1, sc_rad2, energy1
!--------------------------------------------------------------------------------

    select case (force_type)
    case (1)
       ! Machiels forcing (see article in PRL #79(18) p.3411)
       ! if (verbose) write(out,*) "Machiels forcing"; if (verbose) call flush(out)

       energy  = zip
       energy1 = zip

       ! need this normalization factor because the FFT is unnormalized
       fac = one / real(nx**3)**2

       ! assembling the total energy in all forced wavenumbers
       do k = 1, nz
          do j = 1, ny
             do i = 1, nx
                n_shell = nint(sqrt(real(akx(k)**2 + aky(i)**2 + akz(j)**2, 4)))
                if (n_shell .ge. kfmin .and. n_shell .le. kfmax) then
                   fac2 = fac * (fields(i,j,k,1)**2 + fields(i,j,k,2)**2 + fields(i,j,k,3)**2)
                   if (akx(k).eq.zip) fac2 = fac2 * 0.5d0
                   energy1 = energy1 + fac2
                end if
             end do
          end do
       end do

       ! reducing and broadcasting the energy
       count = 1
       call MPI_REDUCE(energy1, energy, count, MPI_REAL8, MPI_SUM, master, MPI_COMM_TASK, mpi_err)
       call MPI_BCAST(energy, count, MPI_REAL8, master, MPI_COMM_TASK, mpi_err)

       ! now applying the forcing to the RHS for velocities (wrk(:,:,:,1:3))

       ! fac is the ratio of the energy that we want to the energy that we have
       ! in the forcing waveband
       fac = force_mag / energy

       do n = 1,n_forced_nodes
          n_shell = k_shell(n)
          i = ifn(n)
          j = jfn(n)
          k = kfn(n)

          wrk(i,j,k,1) = wrk(i,j,k,1) + fac * fields(i,j,k,1)
          wrk(i,j,k,2) = wrk(i,j,k,2) + fac * fields(i,j,k,2)
          wrk(i,j,k,3) = wrk(i,j,k,3) + fac * fields(i,j,k,3)
       end do

    case default
       if (verbose) write(out,*) "WRONG FORCE_TYPE:",force_type
       stop
    end select

    return
  end subroutine force_velocity

!================================================================================
end module m_forcing
