subroutine pressure

  use m_parameters
  use m_fields
  use m_work
  use m_fftw_pencil

  implicit none

  integer :: i, j, k, n
  real (kind=8)  :: div1, div2, lapl1, lapl2, p1, p2

  do k = 1, nz-1, 2
    do j = 1, ny
      do i = 1, nx

        ! getting the divergence (i*k*\hat(u))
        ! remember that in the Fourier space indicies go as (kz,kx,ky)

        ! getting divergence
        div2 =     akx(k  )*fields(i,j,k  ,1) + aky(i)*fields(i,j,k  ,2) + akz(j)*fields(i,j,k  ,3)
        div1 = - ( akx(k+1)*fields(i,j,k+1,1) + aky(i)*fields(i,j,k+1,2) + akz(j)*fields(i,j,k+1,3) )

        ! inverce laplace operator
        lapl1 =  akx(k  )**2 + aky(i)**2 + akz(j)**2
        lapl2 =  akx(k+1)**2 + aky(i)**2 + akz(j)**2

        if (lapl1.eq.0.d0) lapl1 = 9e20
        if (lapl2.eq.0.d0) lapl2 = 9e20

        ! calculating pressure
        p1 = - div1 / lapl1
        p2 = - div2 / lapl2

        ! Taking derivatives of the pressure and subtracting from the corresponding velocities
        fields(i,j,k  ,1) = fields(i,j,k  ,1) + p2 * akx(k+1)
        fields(i,j,k+1,1) = fields(i,j,k+1,1) - p1 * akx(k  )

        fields(i,j,k  ,2) = fields(i,j,k  ,2) + p2 * aky(i)
        fields(i,j,k+1,2) = fields(i,j,k+1,2) - p1 * aky(i)

        fields(i,j,k  ,3) = fields(i,j,k  ,3) + p2 * akz(j)
        fields(i,j,k+1,3) = fields(i,j,k+1,3) - p1 * akz(j)

      end do
    end do
  end do


  return
end subroutine pressure



subroutine divergence

  use m_openmpi
  use m_parameters
  use m_fields
  use m_work
  use m_fftw_pencil

  implicit none

  integer :: i,j,k
  real*8 :: dmin, dmax, d1, d2


  wrk(:,:,:,1:3) = fields(:,:,:,1:3)

  call fft_derivative(1,'x',4)
  call fft_derivative(2,'y',5)
  call fft_derivative(3,'z',6)

  wrk(:,:,:,1) = wrk(:,:,:,4) + wrk(:,:,:,5) + wrk(:,:,:,6)
  call fftw3d(-1,1)
  d1 = minval(wrk(1:nx,:,:,1))
  call MPI_REDUCE(d1,dmin,1,MPI_REAL8,MPI_MIN,0,MPI_COMM_TASK,mpi_err)
  d2 = maxval(wrk(1:nx,:,:,1))
  call MPI_REDUCE(d2,dmax,1,MPI_REAL8,MPI_MAX,0,MPI_COMM_TASK,mpi_err)

  !print *, 'divergence:',myid, d1, d2
  if (verbose) then
    write(out,*) 'divergence:',dmin,dmax
    call flush(out)
  end if

  return
end subroutine divergence



