!==============================================================================!
!
!  Pseudospectral DNS code.  Performs simulation of incompressible isotripic
!  homogeneous turbulence with/without forcing.  Has capability of carrying
!  passive scalars as well.  
!
!  Distributed under the GNU GPLv3 license.
!
!  Copyright (C) 2006-2010 Sergei Chumakov, Natalia Vladimirova, Misha Stepanov
!
!  This program is free software; you can redistribute it and/or modify
!  it under the terms of the GNU General Public License as published by
!  the Free Software Foundation; either version 3 of the License, or
!  (at your option) any later version.
!
!  This program is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
!  GNU General Public License for more details.
!
!  You should have received a copy of the GNU General Public License
!  along with this program; if not, write to the
!    Free Software Foundation, Inc.,
!    51 Franklin Street, Fifth Floor,
!    Boston, MA 02110-1301, USA
!
!==============================================================================!
program x_code_pencils

  use m_parameters
  use m_openmpi
  use m_io
  use m_fields
  use m_work
  use m_fftw_pencil
  use m_forcing
  use m_random
  use m_statistics
  use m_timestep
  use m_timing

  implicit none

  integer :: n
  real*8 :: tmp
  character :: sym

!--------------------------------------------------------------------------------
! Initialization
!--------------------------------------------------------------------------------

  ! Setting the time "zero" for checking the run time
  call m_timing_init   
  ! MPI framework
  call m_openmpi_init
  ! I/O
  call m_io_init
  ! parameters, read the input file
  call m_parameters_init
  ! arrays of variables 
  call m_fields_init
  ! work arrays
  call m_work_init
  ! initialize timestepping mechanism
  call m_timestep_init
  ! start the timing
  call m_timing_restart_init

  ! Alocating and initializing FFT arrays
  call m_fftw_allocate(1)
  call m_fftw_init

  ! statistical quantities
  call m_statistics_init
  ! forcing
  call m_forcing_init

  ! getting the wallclock runlimit
  call get_job_runlimit

  ! starting or restarting
  call begin
  call divergence

!********************************************************************************
!  call benchmark
!********************************************************************************

  ! getting the initial timestep
  call timestep_get_initial_dt

!--------------------------------------------------------------------------------
!  Main cycle starts here
!--------------------------------------------------------------------------------
  do itime = ITMIN + 1, ITMAX

     ! get the current file extension
     call get_file_ext

     ! perform the timestepping for scalars and velocities
     call timestep

     ! advance time
     time = time + dt

     ! compute the next timestep, if it's variable
     if (variable_dt) call timestep_get_new_dt

     ! writing stuff out
     if (mod(itime,iwrite4) == 0) call io_write_4
     if (mod(itime,iwrite8) == 0) call restart_write

     ! check the run-time, make sure we're not running out of it
     call m_timing_check

     ! CPU usage statistics
     if (mod(itime,iprint) == 0) then
        call statistics_main
        sym = " "
        if (mod(itime,iwrite4) == 0) sym = "r"
        if (verbose) then
           write(out,9000) itime,time,dt,courant,cpu_hrs,cpu_min,cpu_sec,sym,dt_info
           call flush(out)
        end if
     end if

     ! writing energy spectrum every timesteps
     ! into a binary file "energy_spectrum.dat"
     !call write_spectrum

     ! termination due to the simulation time
     if (time.gt.tmax) call my_exit(1)

     ! termination due to user (file "stop" exists in the directory)
     inquire(file='stop', exist=there)
     if (there) call my_exit(3)

  end do

!--------------------------------------------------------------------------------
!  End of the main cycle
!--------------------------------------------------------------------------------
!  In a case when we've gone to ITMAX, write the restart file
!--------------------------------------------------------------------------------

  ITIME = ITIME-1
  if (task == 'hydro') call restart_write
  call my_exit(0)

  stop
9000 format('ITIME=',i6,3x,'TIME=',f8.4,4x,'DT=',f8.5,3x,'CFL= ',f6.4, &
       2x,'CPU:(',i4.4,':',i2.2,':',i2.2,')',x,a1,x,a1)

end program x_code_pencils

!================================================================================
!================================================================================
!================================================================================
!================================================================================
!================================================================================
subroutine benchmark
  use m_parameters
  use m_openmpi
  use m_io
  use m_fields
  use m_work
  use m_fftw_pencil
  use m_forcing
  use m_random
  use m_statistics
  use m_timestep
  use m_timing

  implicit none
  integer :: i, j, k, n, nmax
  character :: sym

  ! Benchmarking
  call m_timing_init
  benchmarking = .true.
  bm = 0
  nmax = 1000

  if (verbose) write(out,*) "BENCHMARKING, Hold onto your hats...", nmax
  if (verbose) call flush(out)

  wrk(:,:,:,1) = fields(:,:,:,1)

  do n = 1, nmax

     call system_clock(cpu1,dcpu)
     call FFTW3D(-1,1)
     call system_clock(cpu2,dcpu)
     bm_backward = bm_backward + cpu2 - cpu1

     call system_clock(cpu1,dcpu)
     call FFTW3D( 1,1)
     call system_clock(cpu2,dcpu)
     bm_forward = bm_forward + cpu2 - cpu1
  end do
  call m_timing_check


  if (myid == 0) then
     if (verbose) write(out,*) "BENF: statistics on forward transform"
     if (verbose) write(out,*) "BENF: R2C: ", bm(1)/nmax
     if (verbose) write(out,*) "BENF: T13: ", bm(2)/nmax
     if (verbose) write(out,*) "BENF: C2C: ", bm(3)/nmax
     if (verbose) write(out,*) "BENF: T12: ", bm(4)/nmax
     if (verbose) write(out,*) "BENF: C2C: ", bm(5)/nmax
     if (verbose) write(out,*) "BENF: ====="
     if (verbose) write(out,*) "BENF: TOT: ", bm(11)/nmax
     if (verbose) write(out,*) "BENB: statistics on backward transform"
     if (verbose) write(out,*) "BENB: C2C: ", bm(6)/nmax
     if (verbose) write(out,*) "BENB: T12: ", bm(7)/nmax
     if (verbose) write(out,*) "BENB: C2C: ", bm(8)/nmax
     if (verbose) write(out,*) "BENB: T13: ", bm(9)/nmax
     if (verbose) write(out,*) "BENB: C2R: ", bm(10)/nmax
     if (verbose) write(out,*) "BENB: ====="
     if (verbose) write(out,*) "BENB: TOT: ", bm(12)/nmax
  end if



!--------------------------------------------------------------------------------
!  Profiling of the main cycle starts here
!--------------------------------------------------------------------------------
  bm = 0
  nmax = 200

  ! pretend we don't benchmark so FFT doesn't start to benchmark itself
  benchmarking = .false.

  if (myid == 0) then
     if (verbose) write(out,*) "BENCHMARKING THE MAIN CYCLE...", nmax
     if (verbose) call flush(out)
  end if
  do itime = 1, nmax

     call system_clock(i81,dcpu)
     bm(12) = bm(12) - i81

     ! get the current file extension
     call get_file_ext

     ! ......................................................
     call system_clock(i82,dcpu); bm(1) = bm(1) + i82 - i81; i81 = i82  
     ! ......................................................

     call timestep
     ! ......................................................
     call system_clock(i82,dcpu); bm(3) = bm(3) + i82 - i81  ; i81 = i82  
     ! ......................................................

     ! advance time
     time = time + dt

     ! compute the next timestep, if it's variable
     if (variable_dt) call timestep_get_new_dt

     ! ......................................................
     call system_clock(i82,dcpu); bm(8) = bm(8) + i82 - i81  ; i81 = i82  
     ! ......................................................

     ! writing stuff out
     if (mod(itime,iwrite4) == 0) call io_write_4

     ! ......................................................
     call system_clock(i82,dcpu); bm(9) = bm(9) + i82 - i81  ; i81 = i82  
     ! ......................................................


     ! CPU usage statistics
     if (mod(itime,iprint) == 0) then
        call statistics_main
        call m_timing_check
        sym = " "
        if (mod(itime,iwrite4) == 0) sym = "r"
        if (verbose) write(out,9000) itime,time,dt,courant,cpu_hrs,cpu_min,cpu_sec,&
             sym
        if (verbose) call flush(out)
     end if

     ! ......................................................
     call system_clock(i82,dcpu); bm(10) = bm(10) + i82 - i81  ; i81 = i82  
     ! ......................................................


     ! termination due to the simulation time
     if (time.gt.tmax) call my_exit(1)

     ! termination due to user (file "stop" exists in the directory)
     if (mod(itime,10) == 0) then
        inquire(file='stop', exist=there)
        if (there) call my_exit(3)
     end if

     ! ......................................................
     call system_clock(i82,dcpu); bm(11) = bm(11) + i82 - i81
     bm(12) = bm(12) + i82
     ! ......................................................


  end do

  if (verbose) then
     write(out,*) "Results of benchmarking the main cycle:"
     do n = 1,12
        write(out,"(i3,i10)") n, bm(n)/nmax
     end do
     call flush(out)
  end if



!--------------------------------------------------------------------------------



  call MPI_BARRIER(MPI_COMM_WORLD,mpi_err)

  call my_exit(0)

9000 format('ITIME=',i6,3x,'TIME=',f8.4,4x,'DT=',f8.5,3x,'Courn= ',f6.4, &
       2x,'CPU:(',i4.4,':',i2.2,':',i2.2,')',x,a1)

  stop 
end subroutine benchmark
