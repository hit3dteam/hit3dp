!================================================================================
! M_WORK - module that contains working arrays wrk1....wrk15.
!
! Time-stamp: <2010-02-09 18:33:59 (chumakov)>
!================================================================================
module m_work

!  use m_openmpi
  use m_parameters
  use m_io


  implicit none

  ! --- work arrays
  real*4,allocatable :: tmp4(:,:,:)

  real*8, allocatable :: wrk(:,:,:,:)

  real*8, allocatable :: wrkp(:,:)

  ! this is the flag that signifies whether the sines and cosines 
  ! for the phase shift have been calculated.  Initialized with false.
  logical :: calculated_sines = .false.

  ! the indicies of the work arrays 
  ! nw0 is the first index (0 or 1)
  ! nw4 is the last index
  ! nw(1..3) are truly scratch arrays
  integer :: nw0, nw1, nw2, nw3, nw4

contains

!================================================================================
!================================================================================
!================================================================================
!================================================================================
  subroutine m_work_init

    use m_parameters
    implicit none

    ! different numerical schemes need different number of work arrays


    ! Basic array indicies

    ! In general case and in case of AB scheme,
    ! the required number of arrays is exactly the same
    ! as the number required to obtain the RHS.  it varies with 
    ! dealiasing mechanism: if dealias=1 (phase-shifting), an extra array
    ! is needed to store sine and cosine values for the phase shift. 
    ! these are stored in wrk(nw0)

    nw0 = 0
    if (dealias.eq.0) nw0 = 1
    nw1 = 3 + n_scalars + 1
    nw2 = nw1 + 1
    nw3 = nw2 + 1
    nw4 = nw3

    ! RK 4th order scheme
    ! In this case we need the arrays that are required to calculate the RHS
    ! and then two sets of arrays to store the variables "f_old" and "f_new".
    ! Thus we would take the number of variables from case (0) and add
    ! 2*(n_scalars+3) to the last index (nw4), which is the size of wrk 
    ! array to eb allocated.
    if (NSCHEME.eq.4) nw4 = nw4 + 2*(3+n_scalars)

   

    ! allocating work arrays
    if (task.eq.'hydro')  then

       call m_work_allocate

!!$    elseif (task.eq.'stats')  then
!!$
!!$       call m_work_allocate(6)
!!$
!!$    elseif (task.eq.'parts') then
!!$
!!$       ! required recources are different for the "parts" part of the code
!!$       ! we need several layers of velocities for velocity interpolation
!!$       select case (particles_tracking_scheme)
!!$       case (0)
!!$          allocate(wrk(1:nx,1:ny,1:3,0:0),stat=ierr)
!!$          if (verbose) write(out,*) "Allocated wrk(1:nx+2,1:ny,1:3,0:0)", ierr
!!$          wrk = zip
!!$       case (1)
!!$          allocate(wrk(1:nx,1:ny,1:3,1:3),stat=ierr)
!!$          if (verbose) write(out,*) "Allocated wrk(1:nx+2,1:ny,1:3,1:3)", ierr
!!$          wrk = zip
!!$       case default
!!$          stop 'wrong particles_tracking_scheme'
!!$       end select
!!$       allocate(tmp4(nx,ny,nz), stat=ierr)
!!$       if (verbose) write(out,*) "Allocated tmp4."
!!$       if (verbose) call flush(out)

    else
       if (verbose) write(out,*) 'TASK variable is set in such a way that I dont know how to allocate work arrays'
       if (verbose) write(out,*) 'task = ',task
       call my_exit(-1)
    end if

!!$    if (verbose) write(out,*) "Finished m_work_init"
!!$    if (verbose) call flush(out)

    return
  end subroutine m_work_init

!================================================================================
!================================================================================

  subroutine m_work_allocate

    implicit none
    integer :: number
    integer :: i, ierr, ierr_total

    ierr = 0

    ! array that is needed for output (nx,ny,nz)
    allocate(tmp4(nx,ny,nz),stat=i);   ierr = ierr + i;

    ! main working array, needed for FFT etc, so (nx+2,ny,nz)
    allocate(wrk(nx,ny,nz,nw0:nw4),stat=i);  ierr = ierr + i

    if (ierr.ne.0) then
       print *,'*** WORK_INIT: error in allocation, stopping. ',ierr
       print *,'*** task = ',task
       print *,'*** myid = ',myid
       call my_exit(-1)
       stop
    end if

    if (allocated(wrk) .and. verbose) write(out,"('allocated wrk(nx,ny,nz,',i1,':',i2,')')") &
         LBOUND(wrk,4),uBOUND(wrk,4)
    if (verbose) call flush(out)

    tmp4 = 0.0
    wrk = zip

    return
  end subroutine m_work_allocate

!================================================================================
!================================================================================

  subroutine m_work_exit
    implicit none

    if (allocated(tmp4)) deallocate(tmp4)   
    if (allocated(wrk))  deallocate(wrk)   
    if (allocated(wrkp)) deallocate(wrkp)
    if (verbose) write(out,*) 'deallocated wrk arrays';  if (verbose) call flush(out)

    return
  end subroutine m_work_exit

end module m_work
