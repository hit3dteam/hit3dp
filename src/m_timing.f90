module m_timing

  integer*8 :: cpu0, cpu1, cpu2, dcpu
  integer*4 :: cpu_sec, cpu_min, cpu_hrs, cpu_min_total
  integer*4 :: job_runlimit
  integer*8 :: timing0, timing1, timing2
  integer*4 :: restart_write_duration


!================================================================================

contains
!================================================================================
  subroutine get_job_runlimit

    use m_openmpi
    use m_io
    implicit none

    logical :: there

    ! make the default job runlimit to be 1 hour
    job_runlimit = 1 * 60

    ! make the default job runlimit to be 12 hours
    !job_runlimit = 12 * 60

    ! make the default to be 1 week
    !job_runlimit = 7 * 24 * 60

    ! make the default job runlimit to be 6 months
    !job_runlimit = 6 * 30 * 24 * 60

    if (verbose) write(out,*) 'job_runlimit (default):',job_runlimit

    ! reading the job runlimit (in minutes, supposedly) from the file
    ! job_parameters.txt
    if(myid_world.eq.0) then
       inquire(file='job_parameters',exist=there)
       if (there) then
          open(98,file='job_parameters')
          read(98,*,err=5) job_runlimit
5         close(98)
       end if
    end if
    call MPI_BCAST(job_runlimit,1,MPI_INTEGER4,0,MPI_COMM_WORLD,mpi_err)

    if (verbose) write(out,*) 'Job runlimit (in minutes): ',job_runlimit
    if (verbose) call flush(out)

    return
  end subroutine get_job_runlimit

!================================================================================
!================================================================================

  subroutine m_timing_init

    call system_clock(cpu0,dcpu)

    return
  end subroutine m_timing_init
 

!================================================================================
!================================================================================

  subroutine m_timing_restart_init

    use m_parameters
    implicit none

    ! setting the restart_write_duration - the time it takes (in minutes) to
    ! write a restart file.
    restart_write_duration = 3
    if (nx.lt.3000) restart_write_duration = 1

    return
  end subroutine m_timing_restart_init

!================================================================================
!================================================================================
  subroutine m_timing_check

    
    use m_openmpi
    use m_parameters
    implicit none

    call system_clock(cpu1,dcpu)
    cpu_sec = (cpu1-cpu0)/dcpu
    cpu_min = cpu_sec/60;   cpu_min_total = cpu_min
    cpu_hrs = cpu_min/60
    cpu_min = mod(cpu_min,60)
    cpu_sec = mod(cpu_sec,60)

!!$    call MPI_BCAST(cpu_hrs,1,MPI_INTEGER4,master,MPI_COMM_TASK,mpi_err)
!!$    call MPI_BCAST(cpu_min,1,MPI_INTEGER4,master,MPI_COMM_TASK,mpi_err)

    ! check every 5 iterations if we are getting too close to the 
    ! job run limit time.  If we are closer that twice the time
    ! required for dumping the restart file, we're terminating the run.
    if (mod(itime,5).eq.0) then
       count = 1
       call MPI_BCAST(cpu_min_total, count, MPI_INTEGER4, master, MPI_COMM_TASK, mpi_err)
       if (job_runlimit - cpu_min_total .le. 2*restart_write_duration) call my_exit(2)
    end if

    return
  end subroutine m_timing_check

end module m_timing
