!================================================================================
! MY_EXT - de-initializes everything, called at the end the main program
!          or whenever we need to stop the program anywhere
!
!          reason - the passed variable that describes the particular reason why
!                   we want to stop the program.
!================================================================================
subroutine my_exit(reason)

  use m_openmpi
  use m_io
  use m_fields
  use m_work
!  use x_fftw
!  use m_particles

  implicit none
  integer :: reason
  logical :: file_opened


  if (verbose) write(out,"('my_exit with reason ',i4)") reason

  ! closing the files that might have been opened
  if (myid.eq.0) then
     inquire(unit=990, opened=file_opened)
     if (file_opened) close(990)
  end if


  select case (reason)

  case(0)

     if (verbose) write(out,*) '---------------------------------------------'
     if (verbose) write(out,*) '          NORMAL TERMIATION'
     if (verbose) write(out,*) '---------------------------------------------'

  case(1)

     if (verbose) write(out,*) '---------------------------------------------'
     if (verbose) write(out,*) '           TIME TERMINATION'
     if (verbose) write(out,*) '---------------------------------------------'
     if (verbose) call flush(out)
     call restart_write

  case(2)

     if (verbose) write(out,*) '---------------------------------------------'
     if (verbose) write(out,*) '          RUN-TIME TERMINATION'
     if (verbose) write(out,*) '---------------------------------------------'
     call restart_write
     if (myid.eq.0) call MPI_FILE_DELETE('stop', mpi_info, mpi_err)

  case(3)

     if (verbose) write(out,*) '---------------------------------------------'
     if (verbose) write(out,*) '          USER TERMINATION'
     if (verbose) write(out,*) '---------------------------------------------'
     if (verbose) call flush(out)
     call restart_write

  case default 

     if (verbose) write(out,*) '---------------------------------------------'
     if (verbose) write(out,*) '      TERMINATION FOR NO APPARENT REASON'
     if (verbose) write(out,*) '---------------------------------------------'

  end select


!  if (reason.ge.0) then
!     if (task.eq.'hydro') call restart_write_parallel
!     if (task.eq.'parts') call particles_restart_write_binary
!  end if


  if (verbose) write(out,*) "Done."
  if (verbose) call flush(out)
  close(out)

!  call m_fields_exit
!  call m_work_exit
!  call x_fftw_allocate(-1)
!  call m_io_exit
  call m_openmpi_exit

  stop

  return
end subroutine my_exit

!!$!================================================================================
!!$! MY_INIT - initializes everything, called at the beginning of the main program
!!$!================================================================================
!!$subroutine my_init
!!$
!!$!  --- modules used
!!$  use m_openmpi
!!$  use m_io
!!$  use m_parameters
!!$  use m_fields
!!$  use m_work
!!$  use x_fftw
!!$  use m_filter_xfftw
!!$  implicit none
!!$  integer :: iargc
!!$
!!$!  --- initializing
!!$  call openmpi_init
!!$  call io_init
!!$  call parameters_init
!!$  call fields_init
!!$  call work_init(15)
!!$  call x_fftw_allocate(1)
!!$  call x_fftw_init
!!$
!!$!  --- getting the filter size
!!$  if(iargc().eq.0) then
!!$     call getarg(0,tmp_str)
!!$     if (verbose) write(out,*) 'Format: ',trim(tmp_str),' <filter_size>'
!!$     write(*,*)      'Format: ',trim(tmp_str),' <filter_size>'
!!$     call my_exit(-1)
!!$  end if
!!$
!!$  ftype = 2
!!$  call getarg(1,txt7)
!!$  read(txt7,*) filter_size
!!$  if (verbose) write(out,*) 'Filter size = ',filter_size
!!$  if (verbose) call flush(out)
!!$
!!$  call filter_xfftw_init
!!$
!!$return
!!$end subroutine my_init
