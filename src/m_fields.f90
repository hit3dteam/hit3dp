module m_fields

  implicit none

  real*8, allocatable :: fields(:,:,:,:)

!================================================================================
contains
!================================================================================

  subroutine m_fields_init

    use m_io
    use m_parameters
    implicit none

    integer :: n

    n = 3 + n_scalars

    allocate(fields(nx,ny,nz,n), stat=ierr)
    if (ierr.ne.0) then
       if (verbose) write(out,*) "Cannot allocate fields, stopping."
       call my_exit(-1)
    end if
    fields = zip

    !if (verbose) write(out,"('Allocated ',i4,' fields.')") n
    if (verbose) write(out,*) "Allocated ",n," fields"
    if (verbose) call flush(out)


    return
  end subroutine m_fields_init

!================================================================================

  subroutine m_fields_exit
    use m_io
    implicit none

    if (allocated(fields)) deallocate(fields)

    if (verbose) write(out,*) 'fields deallocated.'
    if (verbose) call flush(out)

    return
  end subroutine m_fields_exit

!================================================================================





end module m_fields


