!================================================================================
!================================================================================
!================================================================================
subroutine init_velocity

  use m_openmpi
  use m_parameters
  use m_io
  use m_fields
  use m_work
  use m_fftw_pencil
  use m_random

  implicit none

  integer :: i, j, k, n
  integer*8 :: seed1, seed2, i8, j8, k8

  integer :: time_array(8)


  real,       allocatable   :: rr(:)
  real*8,     allocatable :: e_spec(:), e_spec1(:)
  integer *8, allocatable :: hits(:), hits1(:)

  integer   :: n_shell
  real*8    :: sc_rad1, sc_rad2

  real*8 :: wmag, wmag2, ratio, fac, fac2

!--------------------------------------------------------------------------------
!  First, if it's a Taylor-Green vortex, then initialize and quit
!--------------------------------------------------------------------------------
  if (isp_type .eq. -1) then
     call init_velocity_taylor_green
     return
  end if

!================================================================================
  allocate( e_spec(kmax), e_spec1(kmax), rr(nx+2), hits(kmax), hits1(kmax), &
       stat=ierr)
  if (ierr.ne.0) stop "cannot allocate the init_velocity arrays"

  if (verbose) write(out,*) 'generating random velocities'
  if (verbose) call flush(out)

!-------------------------------------------------------------------------------
!  Generate the velocities
!-------------------------------------------------------------------------------

  seed1 = RN1
  if (verbose) write(out,*) "RANDOM SEED FOR VELOCITIES = ", seed1
  if (verbose) call flush(out)
  rseed = real(seed1,8)
  fac = random(-rseed)

  ! bringing the processors to their own places in the random sequence
  ! ("6" is there because we're generating six fields
  ! using seed1 because it's int*8
  do i8 = 1, 6*(my_row*nx*nx*nz + my_col*nx*ny)
     fac = random(rseed)
  end do

  ! now filling the arrays wrk1...wrk6 
  ! the trick is to fill it in such a way that it does not 
  ! depend on the number of processors. 
  ! This is done by filling out each (i,j,k) location of the big cube 
  ! (NX by NX by NX) in column-major order.  So we fill one z-slice first, then
  ! skip nx*nx points (6 random number at each point), then fill the next
  ! z-slice
  do k = 1,nz
     do j = 1,ny
        do i = 1,nx
           do n = 1,6
              wrk(i,j,k,n) = random(rseed)
           end do
        end do
     end do
     if (k .ne. nz) then
        do i8 = 1, 6*nx*ny*(pencils_y-1)
           fac = random(rseed)
        end do
     end if
  end do

  ! bringing the random number generators to the same 
  ! point in the sequence again
  do i8 = 1, 6*(pencils_y-my_col-1)*nx*ny
     fac = random(rseed)
  end do

  do i8 = 1, 6*(pencils_z-my_row-1)*nx*nx*nz
     fac = random(rseed)
  end do

  ! debug - printing out the next random number (want to make sure it's the same for all)
  if (verbose) write(out,*) "Random number: ",random(rseed)

  ! making three random arrays with Gaussian PDF 
  ! out of the six arrays that we generated
  wrk(:,:,:,1:3) = sqrt(-two*log(wrk(:,:,:,1:3))) * sin(TWO_PI*wrk(:,:,:,4:6))

  ! --- Making three arrays that have Gaussian PDF and the incompressibility property

  ! go to Fourier space
  do n = 1,3
     call FFTW3D(1,n)
  end do

  ! taking derivatives and linear combination to ensure that the field is solenoidal
  ! assemble the arrays in wrk4..6
  call fft_derivative(3,'y',4)
  call fft_derivative(2,'z',5)
  fields(:,:,:,1) = wrk(:,:,:,4) - wrk(:,:,:,5)
  call fft_derivative(1,'z',5)
  call fft_derivative(3,'x',6)
  fields(:,:,:,2) = wrk(:,:,:,5) - wrk(:,:,:,6)
  call fft_derivative(2,'x',4)
  call fft_derivative(1,'y',3)
  fields(:,:,:,3) = wrk(:,:,:,4) - wrk(:,:,:,3)

  ! maing sure that the initial flow field does not produce any aliasing when multipled in
  ! the physical space
  do k = 1, nz
     do j = 1, ny
        do i = 1, nx
           if (akx(k)**2 + aky(i)**2 + akz(j)**2 .gt. real(kmax)**2) fields(i,j,k,:) = zip
        end do
     end do
  end do


!-------------------------------------------------------------------------------
!     Making the spectrum to be what it should 
!-------------------------------------------------------------------------------

  ! first get the energy spectrum

  ! need this normalization factor because the FFT is unnormalized
  fac = one / real(nx**3,8)**2

  e_spec1 = zip
  e_spec = zip

  ! assembling the total energy in each shell and number of hits in each shell
  do k = 1,nz
     do j = 1,ny
        do i = 1,nx
           n_shell = nint(sqrt(real(akx(k)**2 + aky(i)**2 + akz(j)**2, 4)))
           if (n_shell .gt. 0 .and. n_shell .le. kmax) then
              fac2 = fac * (fields(i,j,k,1)**2 + fields(i,j,k,2)**2 + fields(i,j,k,3)**2)
              if (akx(k).eq.zip) fac2 = fac2 * 0.5d0
              e_spec1(n_shell) = e_spec1(n_shell) + fac2
           end if
        end do
     end do
  end do

  ! reducing the number of hits and energy to two arrays on master node
  count = kmax
  call MPI_REDUCE(e_spec1,e_spec,count,MPI_REAL8,MPI_SUM,0,MPI_COMM_TASK,mpi_err)
  call MPI_BCAST(e_spec,count,MPI_REAL8,0,MPI_COMM_TASK,mpi_err)

!-------------------------------------------------------------------------------
!  Now make the spectrum to be as desired
!-------------------------------------------------------------------------------

  ! first, define the desired spectrum
  do k = 1,kmax

     wmag = real(k, 8)
     ratio = wmag / peak_wavenum

     if (isp_type.eq.0) then
        ! Plain Kolmogorov spectrum
        e_spec1(k) = wmag**(-5.d0/3.d0)

     else if (isp_type.eq.1) then
        ! Exponential spectrum
        e_spec1(k) =  ratio**3 / peak_wavenum * exp(-3.0D0*ratio)

     else if (isp_type.eq.3) then
        ! Von Karman spectrum
        fac = two * PI * ratio
        e_spec1(k) = fac**4 / (one + fac**2)**3

     else
        if (verbose) write(out,*) "ERROR: WRONG INITIAL SPECTRUM TYPE: ",isp_type
        if (verbose) call flush(out)
        stop

     end if
  end do

!  normalize it so it has the unit total energy
  e_spec1 = e_spec1 / sum(e_spec1(1:kmax))

  ! now go over all Fourier shells and multiply the velocities in a shell by
  ! the sqrt of ratio of the resired to the current spectrum
  do k = 1,nz
     do j = 1,ny
        do i = 1,nx

           n_shell = nint(sqrt(real(akx(k)**2 + aky(i)**2 + akz(j)**2, 4)))
           if (n_shell .gt. 0 .and. n_shell .le. kmax .and. e_spec(n_shell) .gt. zip) then
              fields(i,j,k,1:3) = fields(i,j,k,1:3) * sqrt(e_spec1(n_shell)/e_spec(n_shell))
           else
              fields(i,j,k,1:3) = zip
           end if

        end do
     end do
  end do


  if (verbose) write(out,*) "Generated the velocities."
  if (verbose) call flush(out)


  ! deallocate work arrays
  deallocate(e_spec, e_spec1, rr, hits, hits1, stat=ierr)
  return
end subroutine init_velocity

!================================================================================
!  Initialize the velocities with Taylor-Green vortex
!================================================================================
subroutine init_velocity_taylor_green

  use m_openmpi
  use m_parameters
  use m_io
  use m_fields
  use m_work
  use m_fftw_pencil
  use m_random

  implicit none

  integer :: i, j, k, n
  real*8 :: xx, yy, zz

  if (verbose) write(out,*) " --- Initial velocity field is Taylor-Green vortex"
  if (verbose) call flush(out)

  do k = 1, nz
     zz = real(nz*my_row + k - 1, 8) * dz
     do j = 1, ny
        yy = real(ny*my_col + j-1, 8) * dy
        do i = 1, nx
           xx = real(i-1,8) *dx
           wrk(i,j,k,1) =   sin(xx) * cos(yy) * cos(zz)
           wrk(i,j,k,2) = - cos(xx) * sin(yy) * cos(zz)
           wrk(i,j,k,3) = zip
        end do
     end do
  end do

  call FFTW3D(1, 1)
  call FFTW3D(1, 2)
  call FFTW3D(1, 3)
  fields(:,:,:,1:3) = wrk(:,:,:,1:3)

  if (verbose) write(out,*) " --- initialized."
  if (verbose) call flush(out)

  return
end subroutine init_velocity_taylor_green



!================================================================================
!================================================================================

!================================================================================
!================================================================================
!  Initialization of passive scalars
!================================================================================

subroutine init_scalars

  use m_parameters
  use m_io
  use m_fields
  use m_fftw_pencil, only : ialias

  implicit none

  integer :: n_scalar, i, j, k

  if (verbose) write(out,*) 'Generating scalars'
  if (verbose) call flush(out)

  do n_scalar = 1,n_scalars
     call init_scalar(n_scalar)
  end do

  if (verbose) write(out,*) "Generated the scalars."
  if (verbose) call flush(out)

  ! now making sure that the scalars do not have any high
  ! Fourier harmonics by zeroing out everything that has a wavenumber
  ! that potentially can produce aliasing
  do k = 1, nz
     do j = 1, ny
        do i = 1, nx
           if (ialias(i,j,k).gt.0) fields(i,j,k,4:3+n_scalars) = zip
        end do
     end do
  end do

  return

end subroutine init_scalars

!================================================================================
!================================================================================

subroutine init_scalar(n_scalar)

  use m_parameters
  use m_io
  use m_fields

  implicit none

  integer :: n_scalar, ic_type, sc_type

  if (verbose) write(out,*) 'Generating scalar #', n_scalar
  if (verbose) call flush(out)

  sc_type = scalar_type(n_scalar)
  ic_type = sc_type - (sc_type/100)*100

  if (ic_type.eq.0) then   
     ! gradient source - no need for initial conditions
     ! thus making the initial scalar field zero
     if (verbose) write(out,*) "The scalar type = 0, nothing to generate"
     if (verbose) call flush(out)
     fields(:,:,:,n_scalar+3) = zip

  elseif (ic_type.lt.10) then
     ! if the last two digits of the scalar type are less than 10,
     ! the scalar initial conditions are generated based on the
     ! particular spectrum of the scalar
     call init_scalar_spectrum(n_scalar)

  else
     ! if the last two digits are bigger than 10, the scalar is generated
     ! in physical space and then transformed in the Fourier space
     call init_scalar_space(n_scalar)

  end if
  return
end subroutine init_scalar
!================================================================================
!================================================================================
subroutine init_scalar_spectrum(n_scalar)
!================================================================================

  use m_openmpi
  use m_parameters
  use m_io
  use m_fields
  use m_work
  use m_fftw_pencil
  use m_random

  implicit none

  integer :: i, j, k, n, n_scalar
  integer *8 :: i8

  real*8, allocatable :: e_spec(:), e_spec1(:), rr(:)
  integer *8, allocatable :: hits(:), hits1(:)

  integer   :: n_shell
  real*8    :: sc_rad1, sc_rad2

  real*8 :: wmag, wmag2, ratio, fac, rseed2

!--------------------------------------------------------------------------------
  if (verbose) write(out,*) " Generating scalar # ",n_scalar
  if (verbose) call flush(out)

  ! Initializing the random sequence with the seed RN2
  rseed2 = RN2
  fac = random(-rseed2)

  ! allocate work arrays
  allocate( e_spec(kmax), e_spec1(kmax), hits(kmax), hits1(kmax), &
       rr(nx+2), stat=ierr)

  ! bringing the processors to their own places in the random sequence
  ! ("2" is there because we're generating two random number fields
  ! for each scalar field
  ! using i8 because it's int*8
  do i8 = 1, 2*(my_row*nx*nx*nz + my_col*nx*ny)
     fac = random(rseed2)
  end do

  ! now filling the arrays wrk1, wrk2
  do k = 1, nz
     do j = 1, ny
        do i = 1, nx
           wrk(i,j,k,1) = random(rseed2)
           wrk(i,j,k,2) = random(rseed2)
        end do
     end do
     if (k .ne. nz) then
        do i8 = 1, 2*nx*ny*(pencils_y-1)
           fac = random(rseed2)
        end do
     end if
  end do

  ! bringing the random number generators to the same 
  ! point in the sequence again
  do i8 = 1, 2*(pencils_y-my_col-1)*nx*ny
     fac = random(rseed2)
  end do

  do i8 = 1, 2*(pencils_z-my_row-1)*nx*nx*nz
     fac = random(rseed2)
  end do

  ! debug - printing out the next random number (want to make sure it's the same for all)
  if (verbose) write(out,*) "Random number (after initializing scalars): ",random(rseed2)
  if (verbose) call flush(out)

  ! making  random array with Gaussian PDF 
  ! out of the two arrays that we generated
  wrk(:,:,:,3) = sqrt(-two*log(wrk(:,:,:,1))) * sin(TWO_PI*wrk(:,:,:,2))

  ! go to Fourier space
  call FFTW3d(1,3)

!-------------------------------------------------------------------------------
!     Calculating the scalar spectrum
!-------------------------------------------------------------------------------

  ! need this normalization factor because the FFT is unnormalized
  fac = one / real(nx**3,8)**2

  e_spec1 = zip
  e_spec = zip
  hits = 0
  hits1 = 0

  ! assembling the scalar energy in each shell and number of hits in each shell
  do k = 1, nz
     do j = 1, ny
        do i = 1, nx

           n_shell = nint(sqrt(real(akx(k)**2 + aky(i)**2 + akz(j)**2, 4)))
           if (n_shell .gt. 0 .and. n_shell .le. kmax) then
              hits1(n_shell) = hits1(n_shell) + 1
              e_spec1(n_shell) = e_spec1(n_shell) + fac * wrk(i,j,k,3)**2
           end if
        end do
     end do
  end do

  ! reducing the number of hits and energy to two arrays on master node
  count = kmax
  call MPI_REDUCE(hits1,hits,count,MPI_INTEGER8,MPI_SUM,0,MPI_COMM_TASK,mpi_err)
  call MPI_REDUCE(e_spec1,e_spec,count,MPI_REAL8,MPI_SUM,0,MPI_COMM_TASK,mpi_err)

  ! now the master node counts the energy density in each shell
  if (myid.eq.0) then
     fac = four/three * PI / two
     do k = 1,kmax
        sc_rad1 = real(k,8) + half
        sc_rad2 = real(k,8) - half
        if (k.eq.1) sc_rad2 = 0.d0
        if (hits(k).gt.0) then
           e_spec(k) = e_spec(k) / hits(k) * fac * (sc_rad1**3 - sc_rad2**3)
        else
           e_spec(k) = zip
        end if
     end do
  end if

  ! broadcasting the spectrum
  count = kmax
  call MPI_BCAST(e_spec,count,MPI_REAL8,0,MPI_COMM_TASK,mpi_err)

!-------------------------------------------------------------------------------
!  Now make the spectrum to be as desired
!-------------------------------------------------------------------------------
  ! first, define the desired spectrum
  do k = 1,kmax

     wmag = real(k, 8)
     ratio = wmag / peak_wavenum_sc(n_scalar)

     if (scalar_type(n_scalar).eq.0) then
        ! Plain Kolmogorov spectrum
        e_spec1(k) = wmag**(-5.d0/3.d0)

     else if (scalar_type(n_scalar).eq.1 .or. scalar_type(n_scalar).eq.3) then
        ! Exponential spectrum
        e_spec1(k) =  ratio**3 / peak_wavenum_sc(n_scalar) * exp(-3.0D0*ratio)

     else if (scalar_type(n_scalar).eq.2) then
        ! Von Karman spectrum
        fac = two * PI * ratio
        e_spec1(k) = fac**4 / (one + fac**2)**3

     else
        if (verbose) write(out,*) "INIT_SCALARS: WRONG INITIAL SPECTRUM TYPE: ",scalar_type(n_scalar)
        if (verbose) call flush(out)
        stop

     end if
  end do

  ! normalize it so it has the unit total energy
  e_spec1 = e_spec1 / sum(e_spec1(1:kmax))

  ! now go over all Fourier shells and multiply the velocities in a shell by
  ! the sqrt of ratio of the resired to the current spectrum
  fields(:,:,:,3+n_scalar) = zip

  do k = 1, nz
     do j = 1, ny
        do i = 1, nx

           n_shell = nint(sqrt(real(akx(k)**2 + aky(i)**2 + akz(j)**2, 4)))
           if (n_shell .gt. 0 .and. n_shell .le. kmax .and. e_spec(n_shell) .gt. zip) then
              fields(i,j,k,3+n_scalar) = wrk(i,j,k,3) * sqrt(e_spec1(n_shell)/e_spec(n_shell))
           else
              fields(i,j,k,3+n_scalar) = zip
           end if

        end do
     end do
  end do

!-------------------------------------------------------------------------------
!   Creating scalars with double-delta PDF
!-------------------------------------------------------------------------------

  if (scalar_type(n_scalar).eq.3) then
     wrk(:,:,:,nw1) = fields(:,:,:,3+n_scalar)
     call FFTW3d(-1, nw1)

     ! making it double-delta (0.9 and -0.9)
     wrk(:,:,:,nw1) = sign(one,wrk(:,:,:,nw1)) * 0.9d0
     call FFTW3d(1,nw1)

     ! smoothing it by zeroing out high harmonics
     do k = 1, nz
        do j = 1, ny
           do i = 1, nx
              n_shell = nint(sqrt(real(akx(k)**2 + aky(i)**2 + akz(j)**2, 4)))
              if (n_shell .eq. 0 .or. n_shell .ge. kmax*2/3) then
                 wrk(i,j,k,nw1) = zip
              end if
           end do
        end do
     end do
     fields(:,:,:,3+n_scalar) = wrk(:,:,:,nw1)

  end if

  ! deallocate work arrays
  deallocate(e_spec, e_spec1, rr, hits, hits1, stat=ierr)

  return
end subroutine init_scalar_spectrum

!================================================================================
!================================================================================
!================================================================================
!================================================================================
subroutine init_scalar_space(n_scalar)
!================================================================================

  use m_openmpi
  use m_io
  use m_parameters
  use m_fields
  use m_work
  use m_fftw_pencil

  implicit none

  integer :: i, k, n_scalar, sc_type, ic_type, nfi
  real*8  :: zloc, sctmp, h, xx

  nfi = 3 + n_scalar

  if (verbose) write(out,*) " Generating (in X-space) scalar # ", n_scalar
  if (verbose) call flush(out)

  sc_type = scalar_type(n_scalar)
  ic_type = sc_type - (sc_type/100)*100

  select case (ic_type)

!---------------------------------------------------
!  single slab of the scalar
!---------------------------------------------------
  case(11) 

     ! how much to smear out the interface
     ! the minimum interface length is set to 2*dz
     ! the maximum is set to 2*PI/(peak wavenumber for the scalar, taken from
     ! the .in file)
     h = max(2.*dz, two*PI/peak_wavenum_sc(n_scalar))

     ! creating array of scalar
     do i = 1,nx
        xx = dble(i-1) * dx
        sctmp = tanh((xx-PI*0.5)/h) - tanh((xx-PI*1.5)/h) - one
        wrk(i,:,:,nw1) = sctmp
     end do

     ! FFT of the scalar
     call FFTW3d(1,nw1)

     ! putting it into the scalar array
     fields(:,:,:, 3+n_scalar) = wrk(:,:,:,nw1)
     if (iammaster) fields(1,1,1,3+n_scalar) = zip

!---------------------------------------------------
!  two slabs of the scalar
!---------------------------------------------------
  case(12)

     if (verbose) write(out,*) "-- Double-slab scalar in real space"
     if (verbose) call flush(out)

     ! how much to smear out the interface
     h = max(8.*dz, PI/8.d0)

     ! creating array of scalar
     do i = 1,nx
        xx = dble(i-1) * dx
        sctmp = tanh((xx-PI*0.25)/h) - tanh((xx-PI*0.75)/h) + tanh((xx-PI*1.25)/h) - tanh((xx-PI*1.75)/h)
        wrk(i,:,:,nw1) = sctmp - one
        ! now it is between -1 and 1
     end do

     ! FFT of the scalar
     call FFTW3d(1,nw1)

     ! putting it into the scalar array
     fields(:,:,:, 3+n_scalar) = wrk(:,:,:,nw1)

     ! making sure that the mean is ero
     if (iammaster) fields(1,1,1,3+n_scalar) = zip

!---------------------------------------------------
!  N slabs of the scalar
!  actually more like N sinusoidal waves
!---------------------------------------------------
  case(13)
     if (verbose) write(out,*) "-- Multi-slab scalar in real space"
     if (verbose) call flush(out)

     ! making sure that we can support the desired numberof waves with our FFT
     peak_wavenum_sc(n_scalar) = min(peak_wavenum_sc(n_scalar),real(nx/8)) 

     ! definition of initial scalar
     wrk(:,:,:,nfi) = zip
     do i = 1, nx
        wrk(i,:,:,nfi) = sin(peak_wavenum_sc(n_scalar) * dx * real(i-1))
     end do
     call FFTW3d(1,nfi)
     fields(:,:,:,nfi) = wrk(:,:,:,nfi)


!----------------------------------------------------
  case default
     if (verbose) write(out,*) "INIT_SCALARS: UNEXPECTED SCALAR TYPE: ", scalar_type(n_scalar)
     if (verbose) call flush(out)
     stop
  end select

  if (verbose) write(out,*) "Initialized the scalars."
  if (verbose) call flush(out)

  return

end subroutine init_scalar_space
