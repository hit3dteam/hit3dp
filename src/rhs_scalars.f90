subroutine rhs_scalars

  use m_openmpi
  use m_io
  use m_parameters
  use m_fields
  use m_work
  use m_fftw_pencil
  use m_timing

  implicit none

  integer :: i, j, k, n, n1, n2, nv, ns_lo, ns_hi
  real*8  :: rtmp1, rtmp2, wnum2, r11, r12, r21, r22, r31, r32


  ! if not integrating the scalars, return
  if (.not.int_scalars) return


  ! making the RHS for all scalars zero
!  wrk(:,:,:,4:3+n_scalars) = zip

!--------------------------------------------------------------------------------
!  If dealias=0, performing the 2/3 rule dealiasing on scalars
!--------------------------------------------------------------------------------

  if (dealias.eq.0) then

     ! convert the velocities from fields to X-space
     wrk(:,:,:,1:3) = fields(:,:,:,1:3)
     call FFTW3D(-1,1)
     call FFTW3D(-1,2)
     call FFTW3D(-1,3)

     ! Do each scalar one at a time.  Keep the velocities in wrk1:3 intact 
     ! because they are needed later.
     do n = 1, n_scalars

        wrk(:,:,:,nw3) = fields(:,:,:,3+n)
        call FFTW3D(-1,nw3)

        ! Products of the scalar and velocities
        wrk(:,:,:,nw1) = wrk(:,:,:,1) * wrk(:,:,:,nw3)
        wrk(:,:,:,nw2) = wrk(:,:,:,2) * wrk(:,:,:,nw3)
        wrk(:,:,:,nw3) = wrk(:,:,:,3) * wrk(:,:,:,nw3)
        call FFTW3D(1, nw1)
        call FFTW3D(1, nw2)
        call FFTW3D(1, nw3)

        ! Assembling the RHS in wrk(:,:,:,3+n)
        do k = 1, nz-1, 2
           do j = 1, ny
              do i = 1, nx

                 ! If the dealiasing option is 2/3-rule (dealias=0) then we retain the modes
                 ! inside the cube described by $| k_i | \leq  k_{max}$, $i=1,2,3$.
                 ! The rest of the modes is purged

                 if (ialias(i,j,k) .gt. 0) then
                    ! all the wavenumbers that are greater than kmax get zeroed out
                    wrk(i,j,k  ,3+n) = zip
                    wrk(i,j,k+1,3+n) = zip

                 else
                    ! taking the convective term, multiply it by "i" 
                    ! (see how it's done in x_fftw.f90)
                    ! and adding the diffusion term

                    ! also using the fact that the waveunmbers for (i,j,k) are the same
                    ! as wavenumbers for (i,j,k+1)

                    ! i * (a + ib) + d = -b + ia + d
                    rtmp1 =   akx(k+1)*wrk(i,j,k+1,nw1) + aky(i)*wrk(i,j,k+1,nw2) + akz(j)*wrk(i,j,k+1,nw3)
                    rtmp2 =   akx(k  )*wrk(i,j,k  ,nw1) + aky(i)*wrk(i,j,k  ,nw2) + akz(j)*wrk(i,j,k  ,nw3)

                    wnum2 = akx(k)**2 + aky(i)**2 + akz(j)**2

                    wrk(i,j,k  ,3+n) =   rtmp1 - pe(n) * wnum2*fields(i,j,k  ,3+n)
                    wrk(i,j,k+1,3+n) = - rtmp2 - pe(n) * wnum2*fields(i,j,k+1,3+n)

                 end if
              end do
           end do
        end do

     end do

  end if

!--------------------------------------------------------------------------------
!  If dealias=1, performing the phase shift and truncation on scalars
!  The main ideology is as follows.  First we evaluate the phase-shifted
!  quantities, then not phase shifted.  This is done in order to have more
!  working space: at the beginning we have all work arrays available, but at the
!  end we must put sin/cos factors in wrk0 and u.v.w in real space in wrk1...3.
!  They are going to be used again in rhs_velocity later.
!--------------------------------------------------------------------------------
  phase_shifting_dealiasing: if (dealias.eq.1) then

     ! define the sin/cos factors that are used in phase shifting.
     ! computing sines and cosines for the phase shift of dx/2,dy/2,dz/2 
     ! and putting them into wrk0
     if (.not.calculated_sines) then
        do k = 1, nz-1, 2
           do j = 1, ny
              do i = 1, nx
                 wrk(i,j,k  ,0) = cos(half*(akx(k  )+aky(i)+akz(j))*dx)
                 wrk(i,j,k+1,0) = sin(half*(akx(k+1)+aky(i)+akz(j))*dx)
              end do
           end do
        end do
        calculated_sines = .true.
     end if

     ! Doing all scalars at the same time.  We can do this because the
     ! number of work arrays that are available to us is 3+n+3.  First
     ! three later will be taken by  velocities, and the last three are
     ! the primary work arrays with indicies nw1, nw2, nw3.

     ! First, get phase shifted velocities and scalars
     do n = 1, 3 + n_scalars
        ! phase-shifting the quantity
        do k = 1, nz-1, 2
           do j = 1, ny
              do i = 1, nx
                 wrk(i,j,k  ,n) = fields(i,j,k  ,n) * wrk(i,j,k,0) - fields(i,j,k+1,n) * wrk(i,j,k+1,0)
                 wrk(i,j,k+1,n) = fields(i,j,k+1,n) * wrk(i,j,k,0) + fields(i,j,k  ,n) * wrk(i,j,k+1,0)
              end do
           end do
        end do
        ! transforming it to real space
        call FFTW3D(-1,n)
     end do

     ! do one scalar at a time
     phase_shifted_rhs: do n = 4, 3 + n_scalars

        ! getting all three products of phase-shifted scalar and phase-shifted velocities
        ! using three work arrays: n, nw1 and nw2
        wrk(:,:,:,nw1) = wrk(:,:,:,n) * wrk(:,:,:,1)
        wrk(:,:,:,nw2) = wrk(:,:,:,n) * wrk(:,:,:,2)
        wrk(:,:,:,nw3) = wrk(:,:,:,n) * wrk(:,:,:,3)
        ! transforming them to Fourier space
        call FFTW3D(1,nw1)  
        call FFTW3D(1,nw2)  
        call FFTW3D(1,nw3)

        ! phase shifting them back using wrk0 and adding -0.5*(ik) to the RHS for the scalar
        do k = 1, nz-1, 2
           do j = 1, ny
              do i = 1, nx

                 if (ialias(i,j,k) .gt. 1) then
                    wrk(i,j,k  ,n) = zip
                    wrk(i,j,k+1,n) = zip
                 else
                    ! (u+)*(phi+) phase shifted back
                    r11 = wrk(i,j,k  ,nw1) * wrk(i,j,k,0) + wrk(i,j,k+1,nw1) * wrk(i,j,k+1,0)
                    r12 = wrk(i,j,k+1,nw1) * wrk(i,j,k,0) - wrk(i,j,k  ,nw1) * wrk(i,j,k+1,0)
                    ! (v+)*(phi+) phase shifted back
                    r21 = wrk(i,j,k  ,nw2) * wrk(i,j,k,0) + wrk(i,j,k+1,nw2) * wrk(i,j,k+1,0)
                    r22 = wrk(i,j,k+1,nw2) * wrk(i,j,k,0) - wrk(i,j,k  ,nw2) * wrk(i,j,k+1,0)
                    ! (w+)*(phi+) phase shifted back
                    r31 = wrk(i,j,k  ,nw3) * wrk(i,j,k,0) + wrk(i,j,k+1,nw3) * wrk(i,j,k+1,0)
                    r32 = wrk(i,j,k+1,nw3) * wrk(i,j,k,0) - wrk(i,j,k  ,nw3) * wrk(i,j,k+1,0)
                    ! adding -0.5*(ik)*(the result) to the RHSs for the scalar
                    wrk(i,j,k  ,n) = + 0.5d0 * ( akx(k+1)*r12 + aky(i)*r22 + akz(j)*r32 )
                    wrk(i,j,k+1,n) = - 0.5d0 * ( akx(k  )*r11 + aky(i)*r21 + akz(j)*r31 )
                 end if

              end do
           end do
        end do

     end do phase_shifted_rhs

     ! at this moment wrk4...3+n_scalars contain the half of the convective term, which was
     ! obtained from the phase shifted quantities.  Now we need to add the other half of the
     ! convective term (the one that is obtained by multiplication of no-phase-shifted stuff)

     ! first get the velocities into the real space and put them in wrk1...3
     ! this should remain in there untouched, to be used in rhs_velocity later
     wrk(:,:,:,1:3) = fields(:,:,:,1:3)
     call FFTW3D(-1,1)
     call FFTW3D(-1,2)
     call FFTW3D(-1,3)

     ! now do scalars one at a time since we don't have enough storage to do them 
     ! all at once

     not_phase_shifted_rhs: do n = 4, 3 + n_scalars

        ! get the scalar into the real space and put it in the wrk(nw3)
        wrk(:,:,:,nw3) = fields(:,:,:,n)
        call FFTW3D(-1, nw3)

        ! calculate the product of the scalar with velocitiy components
        wrk(:,:,:,nw1) = wrk(:,:,:,nw3) * wrk(:,:,:,1)
        wrk(:,:,:,nw2) = wrk(:,:,:,nw3) * wrk(:,:,:,2)
        wrk(:,:,:,nw3) = wrk(:,:,:,nw3) * wrk(:,:,:,3)
        ! transform it to the Fourier space
        call FFTW3D(1,nw1)
        call FFTW3D(1,nw2)
        call FFTW3D(1,nw3)

        ! add the -0.5*(ik)*results to the RHS, along with the full diffusion term
        do k = 1, nz-1, 2
           do j = 1, ny
              do i = 1, nx

                 if (ialias(i,j,k) .lt. 2) then

                    rtmp1 =   akx(k+1)*wrk(i,j,k+1,nw1) + aky(i)*wrk(i,j,k+1,nw2) + akz(j)*wrk(i,j,k+1,nw3)
                    rtmp2 =   akx(k  )*wrk(i,j,k  ,nw1) + aky(i)*wrk(i,j,k  ,nw2) + akz(j)*wrk(i,j,k  ,nw3)

                    wnum2 = akx(k)**2 + aky(i)**2 + akz(j)**2
                    wrk(i,j,k  ,n) = wrk(i,j,k  ,n) + 0.5d0 * rtmp1 - pe(n-3) * wnum2*fields(i,j,k  ,n)
                    wrk(i,j,k+1,n) = wrk(i,j,k+1,n) - 0.5d0 * rtmp2 - pe(n-3) * wnum2*fields(i,j,k+1,n)

                 end if
              end do
           end do
        end do

     end do not_phase_shifted_rhs

     ! special case - passive scalar with the uniform gradient as a source
     ! adding the source term - the first component of velocity, because we assume
     ! that the uniform gradient has slope 1 and direction in the x-direction
     gradient_source: do n = 1, n_scalars
        if (scalar_type(n) .eq. 0) wrk(:,:,:,n+3) = wrk(:,:,:,n+3) - fields(:,:,:,1)
     end do gradient_source

  end if phase_shifting_dealiasing


  return
end subroutine rhs_scalars


!================================================================================
!================================================================================
!================================================================================
!================================================================================
subroutine test_rhs_scalars

  use m_openmpi
  use m_io
  use m_parameters
  use m_fields
  use m_work
  use m_fftw_pencil

  implicit none

  integer :: i,j,k, n
  real*8 :: a,b,c, x,y,z
  if (task.eq.'hydro') then

     a = 3.d0
     b = 5.d0
     c = 7.d0

     do k = 1,nz
        do j = 1,ny
           do i = 1,nx

              x = dx*real(i-1)
              y = dx*real(my_slab_y*ny + j-1)
              z = dx*real(my_slab_z*nz + k-1)


              wrk(i,j,k,1) = sin(a * x)
              wrk(i,j,k,2) = sin(b * y)
              wrk(i,j,k,3) = sin(c * z)
              wrk(i,j,k,4) = cos(a*x)

           end do
        end do
     end do


     do n = 1,4
        call FFTW3D(1,n)
        fields(:,:,:,n) = wrk(:,:,:,n)
     end do


     nu = .5d0
     pe = 0.5d0
     int_scalars = .true.

     call rhs_scalars

     call FFTW3D(-1,4)


     do k = 1,nz
        do j = 1,ny
           do i = 1,nx

              x = dx*real(i-1)
              y = dx*real(my_slab_y*ny + j-1)
              z = dx*real(my_slab_z*nz + k-1)

              ! checking the RHS
              wrk(i,j,k,1) = -a*cos(two*a*x) - cos(a*x)*(b*cos(b*y)+c*cos(c*z)+a**2*pe(1))

           end do
        end do
     end do

     wrk(:,:,:,1) = abs(wrk(1:nx,:,:,1) - wrk(1:nx,:,:,4))
     print *,'Maximum error is ',maxval(wrk(1:nx,:,:,1))
     call MPI_BARRIER(MPI_COMM_WORLD,mpi_err)     


  end if
  return
end subroutine test_rhs_scalars
