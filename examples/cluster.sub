#PBS -S /bin/bash
#PBS -q regular
#PBS -N 512.42
#PBS -l mppwidth=128
#PBS -l mppnppn=8
#PBS -l walltime=3:00:00
#PBS -e err.txt
#PBS -o out.txt
##PBS -W depend=afterok:6013018
##PBS -m a
##PBS -V

#----------------------------------------------------------------------
# general parameters
#----------------------------------------------------------------------

RUN_NAME=fit_512_42
ITER_LAST=000000
ITER_MAX=999999

SPLIT=none

WORK_DIR=$SCRATCH/dns/512.42
LOG=$WORK_DIR/log.txt

# Program name
PROGRAM=$HOME/research/source/hit3dp/hit3dp.hopper.x

# Job configuration
NUM_CORES=$(qstat -f $PBS_JOBID | grep Resource_List.mppwidth | awk '{print $3}')
NUM_CORES_NODE=$(qstat -f $PBS_JOBID | grep Resource_List.mppnppn | awk '{print $3}')
NUM_CORES_NODE_MAX=$(qstat -f $PBS_JOBID | grep Resource_List.mppnodect | awk '{print $3}')
JOB_TIME=$(qstat -f $PBS_JOBID | grep Resource_List.walltime | awk '{print $3}')
JOB_TIME_MINUTES=$(echo $JOB_TIME | awk -F":" '{print $1*60+$2}')


#if [ $NUM_CORES_NODE -gt $NUM_CODES_NODE_MAX ]; then
#   echo Requested number of cores per node is too big: $NUM_CORES_NODE
#   echo Maximum is $NUM_CODES_NODE_MAX
#   echo Resubmit the job with proper parameters
#   exit 0
#fi

LOADER=aprun
LOADER_OPTS="-n $NUM_CORES -N $NUM_CORES_NODE"

#-----------------------------------------------------------------------
# Function that finds the number of the last dump file
#----------------------------------------------------------------------
find_last_iter()
{
    list_restart_files=( `ls -1 $1.64.?????? 2>/dev/null` )
    n_files=${#list_restart_files[*]}
    if [ $n_files -eq 0 ]
        then
        ITER_LAST=000000
    else
        restart_file=${list_restart_files[$n_files-1]}
        ITER_LAST=`echo $restart_file | sed 's/.*\..*\.\(.*\)/\1/'`
    fi
}

#======================================================================
# MAIN SCRIPT
#======================================================================

cd $WORK_DIR
[ -f stop ] && rm stop

echo $JOB_TIME_MINUTES > job_parameters.txt

echo '=============================================================='>>$LOG
echo "`date +'%D %R'` : JOB $PBS_JOBID started.">>$LOG
echo "`date +'%D %R'` : work_dir : $WORK_DIR">>$LOG
echo "`date +'%D %R'` : run_name : $RUN_NAME">>$LOG

#----------------------------------------------------------------------
# Getting the last dump file number.  If none, set to zero.
#----------------------------------------------------------------------
find_last_iter $RUN_NAME
echo "`date +'%D %R'` : The restart iteration is $ITER_LAST">>$LOG

#----------------------------------------------------------------------
# if the dump number is not smaller than the given ITER_MAX, exit
#----------------------------------------------------------------------
if [ $ITER_LAST -ge $ITER_MAX ]; then
    echo "The maximum iteration number ($ITER_MAX) is acheived.">>$LOG
    echo "Exiting."
    exit 0
fi

#----------------------------------------------------------------------
# if the ITER_MAX is not acheived, need to run the calculation.
# first change the ITMIN and ITMAX in the input file.
#----------------------------------------------------------------------
infile=$RUN_NAME.in
echo "`date +'%D %R'` : Changing the file $infile">>$LOG
sed "s/^\([0-9]\+\)\(.*\)\(ITMIN\)/$ITER_LAST\2\3/g" < $infile > tmp.$$$;
sed "s/^\([0-9]\+\)\(.*\)\(ITMAX\)/$ITER_MAX\2\3/g" < tmp.$$ > $infile;
rm -f tmp.$$


#----------------------------------------------------------------------
# Mailing me that the job is about to start
#----------------------------------------------------------------------
line="[HOP] Job $jobnum <$PBS_JOBNAME> $ITER_LAST:$ITER_MAX started (`date +'%D %R'`)"
mail your@email.com -s "$line" <<EOF

EOF

#----------------------------------------------------------------------
# running the program
#----------------------------------------------------------------------
echo "`date +'%D %R'` : Executing the program...">>$LOG
echo "`date +'%D %R'` : $LOADER $LOADER_OPTS $PROGRAM $RUN_NAME $SPLIT">>$LOG
#for NNN in 128 64 32 16 8 4 2; do
$LOADER $LOADER_OPTS $PROGRAM $RUN_NAME $SPLIT #$NNN
#done

echo "`date +'%D %R'` : ...done.">>$LOG


#exit 0

#----------------------------------------------------------------------
# finding out how many iterations did it run
#----------------------------------------------------------------------
find_last_iter $RUN_NAME
line="[HOP] Job $jobnum <$PBS_JOBNAME> stopped at $ITER_LAST (`date +'%D %R'`)"
gnuplot gnuplot.batch
mail your@email.com -s "$line" -a 1.png -a 2.png <<EOF
Stopped the job.
EOF

# If the run ended with time stop (exit with reason 1), stop
tail -n 20 d0000.txt | grep -q "my_exit with reason    1" && echo "The run is finished." >> $LOG && exit 0

# if there is a file "stop.job" in the directory, quit it!
[ -f stop.job ] && echo "The run was stopped by user." >> $LOG && exit 0

#----------------------------------------------------------------------
# if it's less than iter_max, resubmit the job
#----------------------------------------------------------------------
if [ $ITER_LAST -lt $ITER_MAX ]; then
    echo "`date +'%D %R'` : ITER_MAX ($ITER_MAX) is not acheived ($ITER_LAST)">>$LOG
    echo "`date +'%D %R'` : Submitting another job...">>$LOG
    qsub hopper.sub
    echo "`date +'%D %R'` : Submitted.">>$LOG
fi

