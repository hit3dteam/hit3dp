--------------------------------------------------------------------------------
General info
--------------------------------------------------------------------------------

HIT3DP is a pseudospectral DNS code, that is, it performs direct numerical simulation of incompressible isotripic homogeneous turbulence with or without forcing. The code has capability of carrying passive scalars.  

Future developments may include the following:

- Lagrangian particles
- Large Eddy Simulation capailities

#### Required third-party packages

The code is written in Fortran 90 and uses the two open libraries:

- Open-MPI  (www.open-mpi.org)
- FFTW3	    (www.fftw.org)

#### Licensing info

The code is distributed under the terms of GNU GPLv3 license.  You can read
the full text of the license at http://www.gnu.org/licenses/gpl.html

Copyright (C) 2006-2018 Sergei Chumakov, Natalia Vladimirova, Misha Stepanov

--------------------------------------------------------------------------------

Compiling the code
--------------------------------------------------------------------------------
1.  Edit the Makefile:
    *  define the name of the F90 compiler, unless it's specified by the shell variable $FC
    *  define FCFLAGS and LDFLAGS.  They should include relevant include directories for FFTW3 and relevant flags for MPI and FFTW3
2. Run "make".  The resulting file `hit3dp.x` should appear in the `/src` directory.

--------------------------------------------------------------------------------
Running the code
--------------------------------------------------------------------------------

### Provided examples
The directory "examples" provides samples of the batch job submission files.  

*  01_example.in - an example imput file: small run, decaying turbulence (64^3)
*  02_example.in - an example imput file: medium run, decaying turbulence (512^3)
*  03_example.in - an example imput file: average run, decaying turbulence (1024^3)
*  cluster.lsf - an example batch submission script for LSF queuing system  
*  cluster.sub - a much more elaborate batch submission script for PBS queuing system, with automatic restart/resubmission of the job.

See these files for particular format and options of the running commands (mpirun, etc).

### Starting the simulation
To start a simulation, use a job submission script (see directory /examples)

### Stopping the simulation cleanly
To stop the simulation, create a file "stop" in the run directory (e.g., `touch stop`).  The code will write a restart file and exit.

### Taking into account the time it takes to write a restart file
Restart files can be huge, and it can take minutes to write them. If the time required for restart file write is too long, some batch queueing systems would abort the simulation in the middle of restart write, which is undesirable. To counter this, the code uses the variable `restart_write_duration` specified in `m_timing_restart_init (m_timing.f90)`, which is used in conjunction with `job_runlimit` (see below) to cleanly stop the runs before the prescribed wallclock time runs out.

### Default and prescribed wallclock run time
You can prescribe a particular wallclock runtime `job_runlimit` (in minutes) by specifying it in an (optional) file `job_parameters` (example: `echo "30" > job_parameters)`.  After reaching the wallclock runtime limit the code writes a restart file and exits.

You can change the default wallclock limit in the routine `get_job_runlimit (m_timing.f90)`.

#### Further notes on running simulations

* The number of processes NP must satisfy the following condition:  
  **NP must factor into AxB such that mod(NX,A) = mod(NX,B) = 0**  
  In short, if NX=2^n, then NP should be 2^k for some integer k.
* The parameters "SPLIT" and "NNN" in the executable call are used for debugging/benchmarking purposes only and should not be used.


--------------------------------------------------------------------------------
Input file format
--------------------------------------------------------------------------------
The input file is read by the routine `read_input_file (m_parameters.f90)`. The format follows the file `examples/01_example.in` Parameters are described below.

* **NX** - Number of grid points in one dimension.  
  The computational grid will have NX x NX x NX grid points.  
  The physical dimensions will be 2\*pi x 2\*pi x 2\*pi.  
* **ITMIN** - The timestep number of the restart file.  
  The restart files have names such as "test__this.64.123456".  
  Here, "test\_\_this" is the run name, "64" signifies that the file is written with double precision,  
  "123456" is the timestep number.  
  If the ITMIN is set to 0, the subroutine that defines	the initial conditionis for the flow is called.
* **ITMAX** - The maximum number of timesteps in the simulation.  
  Once the simulation reaches ITMAX timesteps, the code will write restart file and stop.
* **IPRINT** - How often to generate the statistics and write them into .gp files.
* **IWRITE8** - How often to write restart files
* **IWRITE4** - How often to write the real*4 files that are used for post-processing.
* **TMAX** - The maximum simulation runtime (not the wallclocok time)
  Once the simulation reaches time TMAX, the code will write a restart file and stop.
* **TRESCALE** - The time at which to rescale the velocity.  
  This is used in simulations of decaying isotropic turbulence when we want to establish some correlations first and then rescale the velocity field so it has higher kinetic energy.
* **TSCALAR** - Simulation time at which to start moving the passive scalars. 
  Before t=TSCALAR the passive scalar fields remain frozen.
* **flow_type** -  Parameter that switches the flow type  
  0 - decaying turbulence  
  1 - forced turbulence
* **RE** -  The local Reynolds number (1/nu, where nu is viscosity)
* **DT** -  The initial timestep.  
  If DT is negative, then the timestep is fixed to be (-DT)  
  If DT is positive, the timestep is found from the stability criteria for the time-stepping scheme that is used.
* **NSCHEME** - The time-stepping scheme switch.  
  0 - Adams-Bashforth (2nd order in time)  
  4 - Runge-Kutta (4th order in time)  
* **isp_type** -  The energy spectrum for the initial conditions.  
  -1 - Taylor-Green vortices  
  0  - standard kolmogorov -5/3 spectrum  
  1  - Exponential spectrum  
  3  - von Karman spectrum  
  For particularties, see the file `init.f90`
* **ir_exp** - Infrared exponent for the initial spectum (see `init.f90`)
* **peak_wavenum** - The peak wavenumber in the initial spectrum.
* **force_type** - The type of the forcing that is applied for the case of forced turbulence.  
  1 - forcing from Michaels PRL paper (PRL #79(18) p.3411)  
  So far no other forcing has been implemented
* **KFMIN** - The lower bound for the forcing band in the Fourier space.  
* **KFMAX** - The upper bound for the forcing band in the Fourier space.  
* **FORCE_MAG** -The magnitude of the forcing (usually set to 0.5)
* **DEALIAS** - The parameter that switches between  dealiasing algorithms.  
  0 - the standard 3/2-rule (or 2/3 rule).  Faster computations, but fewer modes.  
  1 - the phase shift combined with truncation.  This retains more modes than the 2/3-rule, while increasing the computations 60% or so.  This is the most economical mode for DNS in terms of flops per the number of Fourier modes in the resulting data.
* **det_rand** - The parameter that switches the random generation for the random seeds for the code.  
  **DEFUNCTIONAL**.  In the current version of the code, the seeds for the random number generator are fixed and are taken from the input file. The fixed seeds have the effect of producing the initial data that looks similar for different resolutions (the large features of initial flow in 32^3 simulation will look similar to the large features of a 1024^3 simulation if the seeds are the same).
* The Lagrangian particle section is not implemented in the current version of the code.
* The Large Eddy Simulation capabilities are not implemented in the code at this stage either.
* **NUMS** -  The number of passive scalars to carry around

The last section contains the parameters of the passive scalars.  Each scalar
must have the type, Schmidt number, infrared exponent, peak wavenumber and 
reaction rate.
```
TYPE:
0	The scalar that is forced by the mean gradient.
1-9	The initial conditions for the scalar are generated using Fourier space.
	1: Exponential spectrum
	2: von Karman spectrum
	3: double-delta PDF
>10	The initial conditions for the scalar are generated in the real space.
	11: single slab of the scalar.
	12: two slabs of the scalar
	13: 1-D sinusoidal wave of the scalar
```

The reaction rate parameter is defunctional in this version of the code.

--------------------------------------------------------------------------------
ANY QUESTIONS?
--------------------------------------------------------------------------------
email Sergei Chumakov at sergei@schumakov.info
